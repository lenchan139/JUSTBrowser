package moe.tto.justbrowser.DialogBuilder

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R

class ExitDialogBuilder{
    companion object {
        fun create(activity: MainActivity): AlertDialog {
            val dialog = AlertDialog.Builder(activity).setTitle(String.format(activity.getString(R.string.string_exit_replacer1_questionmark), activity.getString(R.string.app_name)))
                .setPositiveButton(activity.getString(R.string.string_exit)) { dialog, which ->
                    activity.finish()
                    android.os.Process.killProcess(android.os.Process.myPid())
                }.setNegativeButton(activity.getString(R.string.string_cancel), null)
                .create()
            return dialog
        }
    }
}