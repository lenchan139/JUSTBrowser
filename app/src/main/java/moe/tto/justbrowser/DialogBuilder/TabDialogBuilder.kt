package moe.tto.justbrowser.DialogBuilder

import android.R
import android.content.DialogInterface
import android.widget.ArrayAdapter
import moe.tto.justbrowser.MainActivity

class TabDialogBuilder {
    companion object {
        fun create(activity: MainActivity): androidx.appcompat.app.AlertDialog {

            val range = 0..activity.arrGeckoState.size - 1

            val strArray = ArrayList<String>()
            for((index, obj) in activity.arrGeckoState.withIndex()){
                if(index == activity.currentIndexer)
                    strArray.add(String.format(activity.getString(moe.tto.justbrowser.R.string.tag_list_item_current_holder), index.toString(), obj.title))
                else
                    strArray.add(String.format(activity.getString(moe.tto.justbrowser.R.string.tag_list_item_not_current_holder), index.toString(), obj.title))
            }
            val adapter = ArrayAdapter<String>(
                activity,
                R.layout.simple_list_item_1,
                strArray
            )
            val dialog = androidx.appcompat.app.AlertDialog
                .Builder(activity)
                .setTitle(activity.getString(moe.tto.justbrowser.R.string.string_tabs))
                .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                    activity.changeTab(i)


                }).setNegativeButton(activity.getString(R.string.cancel), null)
                .setNeutralButton(activity.getString(moe.tto.justbrowser.R.string.string_new_dotdotdot), DialogInterface.OnClickListener { dialogInterface, i ->
                    activity.addTab()
                })
                .create()

            return dialog
        }
    }
}