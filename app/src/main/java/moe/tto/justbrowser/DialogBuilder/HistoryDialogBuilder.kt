package moe.tto.justbrowser.DialogBuilder

import android.content.DialogInterface
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R

class HistoryDialogBuilder{
    companion object {

        fun create(activity: MainActivity): AlertDialog {
            val historySQLiteController = moe.tto.justbrowser.History.HistorySQLiteController(activity)
            val historyList = historySQLiteController.history
            val listAdapter = object : ArrayAdapter<moe.tto.justbrowser.History.HistoryItem>(
                activity,
                android.R.layout.simple_list_item_2,
                android.R.id.text1,
                historyList){
                    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                        val view = super.getView(position, convertView, parent)
                        val line1 = view.findViewById<TextView>(android.R.id.text1)
                        val line2 = view.findViewById<TextView>(android.R.id.text2)
                        line1.setText(historyList[position].title)
                        line1.setTypeface(line1.typeface, Typeface.BOLD)
                        line2.setText(historyList[position].url)
                        return view
                    }
                }
            val dialog =  AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.string_title_history))
                .setAdapter(listAdapter, DialogInterface.OnClickListener { dialogInterface, i ->
                    activity.loadUrlToGeckoView(historyList[i].url)
                })
                .setNegativeButton(activity.getString(R.string.string_close), null)
                .create()
            return dialog
        }
    }
}