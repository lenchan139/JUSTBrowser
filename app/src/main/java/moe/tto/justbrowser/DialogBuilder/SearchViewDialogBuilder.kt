package moe.tto.justbrowser.DialogBuilder

import moe.tto.justbrowser.MainActivity
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import kotlinx.android.synthetic.main.dialog_search_url.view.*
import moe.tto.justbrowser.History.HistoryItem
import moe.tto.justbrowser.History.HistorySQLiteController
import moe.tto.justbrowser.R




class SearchViewDialogBuilder{
    companion object {

        fun create(activity: MainActivity, defaultUrl: String): AlertDialog {

            val historySQLiteController = HistorySQLiteController(activity)
            // Get the layout inflater
            val inflater = activity.getLayoutInflater()
            val dialogView = inflater.inflate(R.layout.dialog_search_url, null)
            val searchViewInput = dialogView.searchViewInput

            val dialog = AlertDialog
                .Builder(activity)
                .setCancelable(true)
                .setOnCancelListener {
                    it.dismiss()
                }
                .create()

            searchViewInput.requestFocus()
            searchViewInput.setIconifiedByDefault(false)
            val editText = searchViewInput.findViewById<EditText>(R.id.search_src_text)
            val mag_icon = searchViewInput.findViewById<ImageView>(R.id.search_mag_icon)
            val btnClose = dialogView.btnClose
            mag_icon.visibility = View.GONE
            mag_icon.setImageDrawable(null)
            editText.setText(defaultUrl)
            editText.selectAll()

            fun updateSearchViewResultListView(query: String) {

                Log.v("queryTextTracker", String.format("> %s", query))
                val rawResultArray = if (query.isNotEmpty()) {
                    historySQLiteController.getHistoryBySearchUrl(query ?: "")
                } else {
                    historySQLiteController.history
                }

                val adapter = object : ArrayAdapter<HistoryItem>(
                    activity,
                    android.R.layout.simple_list_item_2,
                    android.R.id.text2,
                    rawResultArray
                ) {
                    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
                        val view = super.getView(position, convertView, parent)
                        val line1 = view.findViewById<TextView>(android.R.id.text1)
                        val line2 = view.findViewById<TextView>(android.R.id.text2)

                        line1.setTypeface(line1.typeface, Typeface.BOLD)
                        line1.setText(rawResultArray[position].title)
                        line1.maxLines = 1
                        line2.setText(rawResultArray[position].url)
                        line2.maxLines = 2
                        return view
                    }
                }

                dialogView.searchListView.adapter = adapter
                dialogView.searchListView.setOnItemClickListener { adapterView, view, i, l ->
                    activity.loadUrlToGeckoView(rawResultArray[i].url)
                    dialog.dismiss()
                }

            }

            val onQueryTextList = object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    dialog.dismiss()
                    activity.loadUrlToGeckoView(query)
                    return false
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    return false
                }

            }
            btnClose.setOnClickListener {
                dialog.dismiss()
            }

            searchViewInput.setOnQueryTextListener(onQueryTextList)
            editText.addTextChangedListener {
                val query = it.toString()
                updateSearchViewResultListView(query)
            }
            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            dialog.setView(dialogView)
            updateSearchViewResultListView(defaultUrl)
            return dialog

        }
    }
}