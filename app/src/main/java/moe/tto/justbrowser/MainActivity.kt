package moe.tto.justbrowser

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.evernote.android.state.State
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.dialog_search_url.*
import moe.tto.justbrowser.Delegates.*
import moe.tto.justbrowser.DialogBuilder.ExitDialogBuilder
import moe.tto.justbrowser.DialogBuilder.HistoryDialogBuilder
import moe.tto.justbrowser.DialogBuilder.SearchViewDialogBuilder
import moe.tto.justbrowser.DialogBuilder.TabDialogBuilder
import moe.tto.justbrowser.History.HistorySQLiteController
import moe.tto.justbrowser.Item.SessionStateWithMetadata
import moe.tto.justbrowser.NCBookmark.NCBookmarkMainController
import moe.tto.justbrowser.Utils.*
import org.jetbrains.anko.doAsync
import org.mozilla.gecko.util.BundleEventListener
import org.mozilla.gecko.util.EventCallback
import org.mozilla.gecko.util.GeckoBundle
import org.mozilla.geckoview.*
import java.io.File


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var preferenceUtils: PreferenceUtils
    lateinit var historySQLiteController : HistorySQLiteController
    @State lateinit var geckoRuntime : GeckoRuntime
    @State var arrGeckoState = ArrayList<SessionStateWithMetadata>()
    @State var currentIndexer = -1
    var isGeckoViewLoaded = false
    lateinit var contentUtils: ContentUtils
    val KEY_BUNDLE_SESSION_STATE = "KEY_BUNDLE_SESSION_STATE"
    // for file chooser
    val fileChooserRequestCode = 1902
    var fileChooserCallBack : GeckoSession.PromptDelegate.FileCallback? = null
    var shouldExecFileChooserCallback = false
    var lastTitle = ""
    //end of file chooser
    var externalUrl : String = ""
    var uniqueDialog : AlertDialog? = null
    lateinit var adBlocker : AdBlocker
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        contentUtils = ContentUtils(this@MainActivity)
        adBlocker = AdBlocker.getInstance(this@MainActivity)
        try {
            val contentBlocking = ContentBlocking.Settings.Builder()
                .categories(ContentBlocking.AT_SOCIAL)
                .categories(ContentBlocking.AT_CONTENT)
                .categories(ContentBlocking.AT_ANALYTIC)
                .categories(ContentBlocking.AT_AD)
                .categories(ContentBlocking.AT_ALL)
                .categories(ContentBlocking.SB_UNWANTED)
                .categories(ContentBlocking.SB_PHISHING)
                .categories(ContentBlocking.SB_MALWARE)
                .categories(ContentBlocking.SB_HARMFUL)
                .build()

            val geckoRuntimeSettings = GeckoRuntimeSettings.Builder()
                .contentBlocking(contentBlocking)
                .build()
            geckoRuntime = GeckoRuntime.create(this@MainActivity, geckoRuntimeSettings)
        }catch (e:IllegalStateException){
            e.printStackTrace()
            restartThisActivityPendentingIntent()
        }
        preferenceUtils = PreferenceUtils(this@MainActivity)
        historySQLiteController = HistorySQLiteController(this@MainActivity)
        getWindow()
            .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        tryCatchExternalUrl(intent, false)
        if(savedInstanceState == null){
            initTabsController()

        }

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        NCBookmarkMainController(this)

        nav_view.setNavigationItemSelectedListener(this)
        initFabButton()
        initSwtichTabButton()
        initEdtTextUrlHolder()
        permissionChecker()

        initRefresher()
        initTestFunc()
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ncbookmark_icon_on_drawer)
    }
    fun initTestFunc(){
        geckoView.eventDispatcher.registerUiThreadListener(object: BundleEventListener{
            override fun handleMessage(event: String?, message: GeckoBundle?, callback: EventCallback?) {
                Log.v("onLoadUri", "event:${event}\nmessage:${message.toString()}")
            }

        } )
    }
    override fun onPause() {
        super.onPause()
        saveCurrentState {}
    }

    override fun onResume() {
        super.onResume()
        initCurrentSession()
        updateSwitchCount()
    }

    override fun onPostResume() {
        super.onPostResume()
        reloadAdBlockerPreference()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }else if((getCurrentSession().navigationDelegate as moe.tto.justbrowser.Delegates.GeckoNavigationDelegateOverride).canGoBack){
            getCurrentSession().goBack()
        }else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        // create for mertirial search view

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings ->  startActivity(Intent(this@MainActivity, SettingsActivity::class.java))
            R.id.action_homepage -> getCurrentSession().loadUri(preferenceUtils.getHomepage())
            R.id.action_show_history -> showHistoryDialog()
            R.id.action_share_current -> shareCurrPage()
            R.id.action_open_with -> runToExternal(arrGeckoState[currentIndexer].url)
            R.id.action_refresh_current -> getCurrentSession().reload()
            R.id.action_switch_tab -> showTabDialog()
            R.id.action_exit -> showExitDialog()
            R.id.action_power_saving_exception -> requestPowerSavingException()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == fileChooserRequestCode && resultCode == Activity.RESULT_OK){
            shouldExecFileChooserCallback = true
            doAsync {
                execFileChooserCallback(data)
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        tryCatchExternalUrl(intent, true)
    }
    fun tryCatchExternalUrl(intent:Intent?, isActivityCreated:Boolean){
        if(intent == null ) return

        //if from crash, change url
        val isFromCrash = preferenceUtils.getIsFromCrash()
        if(isFromCrash){
            externalUrl = preferenceUtils.getLastBrowseUrl()
        }
        if(externalUrl.isNotEmpty()) return
        val keyInURLFromExternal = getString(R.string.intentExtra_key_InURLFromExternal)
        val inUrl = intent.getStringExtra(getString(R.string.key_stringExtra_InURL)) ?: intent.data?.toString()
        val isFromExternal = intent.getBooleanExtra(keyInURLFromExternal, true)
        if(inUrl == null) {
            super.onNewIntent(intent)
        }else if(isFromExternal && isActivityCreated){
            externalUrl = inUrl
            addTab()
            loadUrlToGeckoView(inUrl)
            intent.putExtra(keyInURLFromExternal,false)
        }else if(isFromExternal){
            externalUrl = inUrl
            intent.putExtra(keyInURLFromExternal,false)
        }else {
            externalUrl = inUrl
        }
    }
    //view initer
    fun initRefresher(){
        refresh_geckoview.setOnRefreshListener {
            getCurrentSession().reload()

        }
        refresh_geckoview.isRefreshing = false
        refresh_geckoview.setOnChildScrollUpCallback { parent, child ->
            val t = (getCurrentSession().scrollDelegate as GeckoScrollDelegateOverride).scrollY > 0
            Log.v("onChlidScrollUpChange", t.toString())
            (getCurrentSession().scrollDelegate as GeckoScrollDelegateOverride).scrollY > 0
        }
    }
    fun initSwtichTabButton(){
        btnSwitchTabs.setOnClickListener {
            showTabDialog()
        }
    }

    //end of view init func
    // for fab button
    fun initFabButton(){
        fab.setOnClickListener { view ->
            showTabDialog()
        }
        fab.setOnLongClickListener {

            true
        }
        fab.visibility = View.GONE
    }
    //end of fab button
    // for url holder view
    fun initEdtTextUrlHolder(){
        edittextUrlHolder.setOnClickListener {
            showSearchViewDialog()
        }
        edittextUrlHolder.setOnLongClickListener {
            showEdtTextUrlLongClickDialog()
            true
        }
    }
    fun showEdtTextUrlLongClickDialog(){
        val array = arrayOf("Parse & Go", "Copy Link")
        val adapter = ArrayAdapter<String>(
            this@MainActivity,
            android.R.layout.simple_list_item_1,
            array)

        AlertDialog.Builder(this)
            .setTitle(edittextUrlHolder.text)
            .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                if(i == 0){
                    val clipText = contentUtils.copyFromClipboard(edittextUrlHolder.text.toString())
                    if(clipText.isNotEmpty()){
                        loadUrlToGeckoView(clipText)
                    }
                }else if(i == 1){
                    contentUtils.copyToClipboard(edittextUrlHolder.text.toString())
                }
            }).create()
            .show()
    }
    // end of url holder view
    // basic control func
    fun loadUrlToGeckoView(inUrl:String?){
        if(inUrl == null) return
        var url = inUrl
        if(url.isValidUrl()){
        }else if((!url.contains(".") || url.contains(" "))
            || (!url.contains(" ") && !url.contains("."))
            || (url.contains(" ") && url.indexOf(" ") < url.indexOf("."))){
            // it is keywords! do magic
            url = String.format(preferenceUtils.getSearchEngineReplacer(), url)
        }else if (!url.toLowerCase().startsWith("http:") || url.toLowerCase().startsWith("https:")){
            // if not http protocol, do something
            url = String.format("http://%s", url)
        }
        getCurrentSession().loadUri(url)
        if(drawer_layout.isDrawerOpen(GravityCompat.START))
            drawer_layout.closeDrawers()
    }
    fun showTabDialog():Boolean{
        val dialog = TabDialogBuilder.create(this@MainActivity)
        showUniqueDialog(dialog)
        return true
    }
    // end of basic control func

    //start for SearchView
    fun showSearchViewDialog(){
        val dialog = SearchViewDialogBuilder.create(this@MainActivity, arrGeckoState[currentIndexer].url)
        showUniqueDialog(dialog)

        uniqueDialog!!.searchViewInput.requestFocus()

    }

    //end for MeritialSearchView


    // start of tabs controller
    fun updateSwitchCount(){
        btnSwitchTabs.text = arrGeckoState.size.toString()
    }
    fun initCurrentSession(){
        val session = geckoView.session
            if(session!= null && !session.isOpen){
                session.open(geckoRuntime)
                initSessionDelegates(session)
                val state = getCurrentState()
                if(state != null)
                    session.restoreState(state)
                else
                    session.loadUri(preferenceUtils.getHomepage())


        }
    }
    fun reinitCurrentSession(){
        saveCurrentState {
            val session = GeckoSession()
            session.open(geckoRuntime)
            initSessionDelegates(session)
            loadSessionInitUrl(session)
            try{
                geckoView.releaseSession()
            }catch(e:java.lang.Exception){}
            geckoView.setSession(session)
        }
    }
    fun initSessionDelegates(session:GeckoSession){
        session.progressDelegate = GeckoProgressDelegatesOverride(this@MainActivity)
        session.navigationDelegate = GeckoNavigationDelegateOverride(this@MainActivity)
        //session.selectionActionDelegate = GeckoSelectionDelegateOverride(this@MainActivity)
        session.contentDelegate = GeckoContentDelegateOverride(this@MainActivity)
        session.promptDelegate = GeckoPromptDelegateOverride(this@MainActivity)
        session.scrollDelegate = GeckoScrollDelegateOverride(this@MainActivity)
        //session.contentBlockingDelegate = GeckoContentBlockedDelegateOverride(this@MainActivity)
    }
    fun initTabsController(){
        val geckoSessionSettings = GeckoSessionSettings.Builder()
            .useMultiprocess(true)
            .suspendMediaWhenInactive(true)
            .build()
        val session = GeckoSession(geckoSessionSettings)
        val state = getCurrentState()
        initSessionDelegates(session)
        session.open(geckoRuntime)

        if(state != null) {
            getCurrentSession().restoreState(state)
        }else {
            loadSessionInitUrl(session)
            arrGeckoState.add(SessionStateWithMetadata(null, "", ""))
            geckoView.setSession(session)
        }
        currentIndexer = 0
        updateSwitchCount()
    }
    fun loadSessionInitUrl(session:GeckoSession){
        val lastStateUrl = preferenceUtils.getLastBrowseUrl()
        if(externalUrl.isNotEmpty()){
            session.loadUri(externalUrl)
            externalUrl = ""
        }else if(lastStateUrl.isNotEmpty()) {
            session.loadUri(lastStateUrl)
        }else{
            session.loadUri(preferenceUtils.getHomepage())
        }
    }
    fun getCurrentSession():GeckoSession{
        return geckoView.session ?: GeckoSession()
    }
    fun getCurrentState():GeckoSession.SessionState?{
        return try{ arrGeckoState.get(currentIndexer).state } catch(e:ArrayIndexOutOfBoundsException){ null }
    }
    fun getCurrentTitle():String{
        return lastTitle
    }
    fun getCurrentUrl():String{
        return edittextUrlHolder.text.toString()
    }
    fun saveCurrentState(callback: ()->Unit):Boolean{
        if(arrGeckoState.size > currentIndexer){

                getCurrentSession()
                    .saveState()

                    .then(GeckoResult.OnValueListener<GeckoSession.SessionState, Void> { state ->
                        arrGeckoState[currentIndexer].state = state
                        callback()
                null
            })
            return true
        }else{
            return false
        }
    }
    fun restoreCurrentState():Boolean{
        if(getCurrentState() != null)
            getCurrentSession().restoreState(getCurrentState()!!)
        return getCurrentState() != null
    }
    fun changeTab(index : Int){
        saveCurrentState {
            if(currentIndexer != index && index < arrGeckoState.size) {
                // change index
                currentIndexer = index
                // if no state, re-init
                if(restoreCurrentState()){
                }else{
                    reinitCurrentSession()
                }

            }
            // after
            updateSwitchCount()
        }
    }
    fun addTab(){
        saveCurrentState {

            // add sessionState to array
            arrGeckoState.add(SessionStateWithMetadata(null, "", ""))
            currentIndexer = arrGeckoState.size - 1
            reinitCurrentSession()
            //after
            updateSwitchCount()
        }
    }
    fun removeTab(index:Int){
        arrGeckoState.removeAt(index)
        if(currentIndexer >= index){
            currentIndexer = currentIndexer - 1
        }
        restoreCurrentState()
        updateSwitchCount()
    }

    // end of tab controll

    fun showHistoryDialog():Boolean{
        val dialog = HistoryDialogBuilder.create(this@MainActivity)
        showUniqueDialog(dialog)
        return true
    }
    fun permissionChecker(){
        try {
            if (ContextCompat.checkSelfPermission(this@MainActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.
                    Toast.makeText(this, getString(R.string.dialog_string_message_ask_storage_permission), Toast.LENGTH_LONG).show()
                    val dialog = AlertDialog.Builder(this)
                    dialog.setTitle(getString(R.string.dialog_string_title_storage_access_requred))
                        .setMessage(getString(R.string.dialog_string_message_ask_storage_permission))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.string_grant), DialogInterface.OnClickListener { dialogInterface, i ->
                            val STORAGE_PERMISSION_ID = 112
                            ActivityCompat.requestPermissions(this@MainActivity,
                                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                STORAGE_PERMISSION_ID)

                        }).create().show()
                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, getString(R.string.string_lower_android5_warning), Toast.LENGTH_SHORT).show()
        }


    }


    fun shareCurrPage(): Boolean {

        val sendIntent = Intent()
        val sfType =  preferenceUtils.getShareBodyFormat()
        val titleLimiter = preferenceUtils.getShareTitleLimitSize()
        val title = getCurrentTitle().takeLeft(titleLimiter)
        val url = getCurrentUrl()
        if(sfType == 1){
            val content = String.format("%s\n%s", title, url)
            sendIntent.putExtra(Intent.EXTRA_TEXT, content)
        }else if(sfType == 2){
            sendIntent.putExtra(Intent.EXTRA_TEXT, url)
        }else {
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, title)
            sendIntent.putExtra(Intent.EXTRA_TEXT, url)
        }
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.type = "text/plain"
        startActivity(Intent.createChooser(sendIntent, getString(R.string.string_sent_to_dotdotdot)))
        return true
    }

    fun openIntentSchemeUrl(url:String): Boolean{
        if(url.startsWith(getString(R.string.prefix_intent_scheme))){
            try {
                val intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
                val existPackage = packageManager.getLaunchIntentForPackage(intent.getPackage())
                if (existPackage != null) {
                    openIsOpenIntentDIalog(intent)
                } else {
                    val marketIntent = Intent(Intent.ACTION_VIEW)
                    marketIntent.data = Uri.parse(getString(R.string.prefix_market_url_for_application) + intent.getPackage())
                    openIsOpenIntentDIalog(intent)
                }
                return true
            }catch (e:java.lang.Exception){
                e.printStackTrace()
            }
        }
        return false
    }


    fun openIsOpenIntentDIalog(intent:Intent){
        val dialog = AlertDialog.Builder(this)
        val theIntent = packageManager.queryIntentActivities(intent, 0).get(0)
        dialog.setTitle(getString(R.string.string_open_in_prefix) + theIntent.loadLabel(packageManager) + getString(R.string.string_open_in_postfix))
            .setIcon(theIntent.loadIcon(packageManager))
            .setNegativeButton(getString(R.string.string_cancel), DialogInterface.OnClickListener { dialogInterface, i ->

            })
            .setPositiveButton(getString(R.string.string_go), DialogInterface.OnClickListener { dialogInterface, i ->
                startActivity(intent)
            }).create().show()
    }

    fun decideOpenExternalButton(url:String?){
        if(url == null || url.isEmpty()){
            btnOpenExternal.visibility = View.GONE
            btnOpenExternal.setOnClickListener {}
        }else{
            btnOpenExternal.visibility = View.VISIBLE
            btnOpenExternal.setOnClickListener {
                runToExternal(url)
            }
        }
    }

    fun runToExternal(url: String) : Boolean {
        // if intent uri, open it and igore below all code
        if (openIntentSchemeUrl(url))
            return false
        // beginning
        val preIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        val targetedShareIntents = ArrayList<Intent>()
        val browserIntent = Intent.createChooser(preIntent, getString(R.string.string_open_with_dotdotdot))
        val resInfo = packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_ALL)

        for (resolveInfo in resInfo) {
            Log.v("listV", resolveInfo.activityInfo.packageName)
            val packageName = resolveInfo.activityInfo.packageName
            val targetedShareIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            targetedShareIntent.setPackage(packageName)
            if (!packageName.contains(applicationContext.packageName)) {
                targetedShareIntents.add(targetedShareIntent)
                Log.v("listVTureFalse", "True")
            }
        }
        if (targetedShareIntents.size > 1) {
            val chooserIntent = Intent.createChooser(
                targetedShareIntents.removeAt(targetedShareIntents.size - 1), getString(R.string.string_open_with_dotdotdot))

            chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS, JavaUtils().listToPracelable(targetedShareIntents))
            startActivity(chooserIntent)
        } else if (targetedShareIntents.size == 1) {
            openIsOpenIntentDIalog(targetedShareIntents.get(0))
        } else {
            Toast.makeText(this,getString(R.string.string_no_handler_here_dot)
                , Toast.LENGTH_SHORT).show()
        }
        return true
    }
    fun showExitDialog():Boolean{
        val dialog = ExitDialogBuilder.create(this@MainActivity)
        showUniqueDialog(dialog)
        preferenceUtils.setLastBrowseUrl(null)
        return true
    }

    // for file chooser
    fun openFileChooser(mimeType:String?, callback:GeckoSession.PromptDelegate.FileCallback?, multiple: Boolean){
        val intent = Intent()
            .setType(mimeType)
            .setAction(Intent.ACTION_GET_CONTENT)
        if(multiple)
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        fileChooserCallBack = callback
        startActivityForResult(Intent.createChooser(intent, getString(R.string.string_select_a_file_colon)), fileChooserRequestCode)

    }
    fun execFileChooserCallback(intent:Intent?){
        if(intent != null) {
            if (intent.clipData != null) {
                val count = intent.clipData!!.itemCount
                var currentItem = 0
                val arrUri = ArrayList<Uri>()
                for (i in 0..count - 1) {
                    val preUri = intent.clipData!!.getItemAt(currentItem).uri
                    val filepath = FileUtils.getPath(this@MainActivity, preUri)
                    val uri = File(filepath).toUri()
                    arrUri.add(uri)
                    currentItem = currentItem + 1
                }

                // run callback for multiple
                fileChooserCallBack?.confirm(this@MainActivity, arrUri.toTypedArray())
            } else if (intent.data != null) {
                val preUri = intent.data!!
                val filepath = FileUtils.getPath(this@MainActivity, preUri)
                val uri = File(filepath).toUri()

               //run callback for single
                fileChooserCallBack?.confirm(this@MainActivity, uri)
            }
        }
    }
    // end of file chooser


    // request add power-saving exception func
    fun requestPowerSavingException(){
        val intent = Intent()
        val packageName = getPackageName()
        val pm : PowerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
        if (pm.isIgnoringBatteryOptimizations(packageName))
            intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
        else {
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + packageName));
        }
        startActivity(intent)
    }

    fun restartThisActivityPendentingIntent(){
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(getString(R.string.key_start_from_app_crash), true)
        startActivity(intent)
    }

    fun showUniqueDialog(dialog:AlertDialog){
        if(uniqueDialog != null && uniqueDialog!!.isShowing)
            uniqueDialog!!.dismiss()
        uniqueDialog = dialog
        uniqueDialog!!.show()
        uniqueDialog!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }
    fun showKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
    }
    fun hideKeyboard(){
        val imm =  getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = getCurrentFocus()
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
    }
    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    fun reloadAdBlockerPreference(){
        adBlocker.reloadBlockedHostFile(this@MainActivity)
        adBlocker.setAdblockEnabled(preferenceUtils.isEnableAdBlocker)
        adBlocker.updatePreference()
    }
}
