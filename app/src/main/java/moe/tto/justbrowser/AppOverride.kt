package moe.tto.justbrowser


import android.app.Application
import android.content.Context
import com.evernote.android.state.StateSaver
import android.content.Intent
import android.content.Context.MODE_PRIVATE





class AppOverride : Application() {
    val CrashTag = "CrashTag129301"
    val KEY_APP_CRASHED = "KEY_APP_CRASHED"
    override fun onCreate() {
        super.onCreate()
        StateSaver.setEnabledForAllActivitiesAndSupportFragments(this, true)

        val defaultHandler = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler(object : Thread.UncaughtExceptionHandler {
            override fun uncaughtException(thread: Thread, exception: Throwable) {
                // Save the fact we crashed out.
                getSharedPreferences(CrashTag, Context.MODE_PRIVATE).edit()
                    .putBoolean(KEY_APP_CRASHED, true).apply()
                // Chain default exception handler.
                defaultHandler?.uncaughtException(thread, exception)
            }
        })

        val bRestartAfterCrash = getSharedPreferences(CrashTag, Context.MODE_PRIVATE)
            .getBoolean(KEY_APP_CRASHED, false)
        if (bRestartAfterCrash) {
            // Clear crash flag.
            //getSharedPreferences(TAG, Context.MODE_PRIVATE).edit()
                //.putBoolean(KEY_APP_CRASHED, false).apply()
            // Re-launch from root activity with cleared stack.
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(getString(R.string.key_start_from_app_crash), true)
            //startActivity(intent)
        }
    }
}