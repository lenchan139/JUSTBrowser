package moe.tto.justbrowser.Delegates

import kotlinx.android.synthetic.main.content_main.*
import moe.tto.justbrowser.MainActivity
import org.mozilla.geckoview.GeckoSession

class GeckoScrollDelegateOverride(val activity: MainActivity) : GeckoSession.ScrollDelegate{
    var scrollX = 0
    var scrollY = 0
    override fun onScrollChanged(session: GeckoSession, scrollX: Int, scrollY: Int) {
        this.scrollX = scrollX
        this.scrollY = scrollY
    }

}