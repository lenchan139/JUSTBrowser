package moe.tto.justbrowser.Delegates

import android.app.AlertDialog
import android.content.DialogInterface
import org.mozilla.geckoview.GeckoResult
import org.mozilla.geckoview.GeckoSession
import android.widget.LinearLayout
import android.widget.EditText
import org.mozilla.geckoview.AllowOrDeny
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R


class GeckoPromptDelegateOverride(val activity: MainActivity) : GeckoSession.PromptDelegate{

    override fun onDateTimePrompt(
        session: GeckoSession,
        title: String?,
        type: Int,
        value: String?,
        min: String?,
        max: String?,
        callback: GeckoSession.PromptDelegate.TextCallback
    ) {

    }

    override fun onFilePrompt(
        session: GeckoSession,
        title: String?,
        type: Int,
        mimeTypes: Array<out String>?,
        callback: GeckoSession.PromptDelegate.FileCallback
    ) {
        val multiple = (type == GeckoSession.PromptDelegate.FILE_TYPE_MULTIPLE)
        var mimeType = "*/*"
        if(mimeTypes != null)
         if(mimeTypes.size > 0)
            mimeType = mimeTypes.get(0)
        activity.openFileChooser(mimeType, callback, multiple)

    }

    override fun onColorPrompt(
        session: GeckoSession,
        title: String?,
        value: String?,
        callback: GeckoSession.PromptDelegate.TextCallback
    ) {
    }

    override fun onAuthPrompt(
        session: GeckoSession,
        title: String?,
        msg: String?,
        options: GeckoSession.PromptDelegate.AuthOptions,
        callback: GeckoSession.PromptDelegate.AuthCallback
    ) {
    }

    override fun onChoicePrompt(
        session: GeckoSession,
        title: String?,
        msg: String?,
        type: Int,
        choices: Array<out GeckoSession.PromptDelegate.Choice>,
        callback: GeckoSession.PromptDelegate.ChoiceCallback
    ) {
    }

    override fun onAlert(
        session: GeckoSession,
        title: String?,
        msg: String?,
        callback: GeckoSession.PromptDelegate.AlertCallback
    ) {
        AlertDialog.Builder(activity)
            .setNegativeButton(activity.getString(R.string.string_ok), null)
            .setTitle(String.format(activity.getString(R.string.prompt_string_web_say_colon_replace1), title))
            .setMessage(String.format("%s", msg))
            .create()
            .show()
    }

    override fun onTextPrompt(
        session: GeckoSession,
        title: String?,
        msg: String?,
        value: String?,
        callback: GeckoSession.PromptDelegate.TextCallback
    ) {

        val input = EditText(activity)
        input.setText(value)
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        input.layoutParams = lp
        AlertDialog.Builder(activity)
            .setView(input)
            .setTitle(String.format(activity.getString(R.string.prompt_string_web_say_colon_replace1), title))
            .setMessage(String.format("%s", msg))
            .setNegativeButton(activity.getString(R.string.string_cancel), null)
            .setPositiveButton(activity.getString(R.string.string_done), DialogInterface.OnClickListener { dialogInterface, i ->
                callback?.confirm(input.text.toString())
            })
            .create()
            .show()
    }
    override fun onPopupRequest(session: GeckoSession, targetUri: String?): GeckoResult<AllowOrDeny>? {
        return GeckoResult()
    }

    override fun onButtonPrompt(
        session: GeckoSession,
        title: String?,
        msg: String?,
        btnMsg: Array<out String>?,
        callback: GeckoSession.PromptDelegate.ButtonCallback
    ) {
    }


}