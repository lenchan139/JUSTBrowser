package moe.tto.justbrowser.Delegates

import android.util.Log
import org.mozilla.geckoview.GeckoResponse
import org.mozilla.geckoview.GeckoSession
import moe.tto.justbrowser.MainActivity

class GeckoSelectionDelegateOverride(val activity: MainActivity) : GeckoSession.SelectionActionDelegate{
    override fun onHideAction(session: GeckoSession, reason: Int) {

    }

    override fun onShowActionRequest(
        session: GeckoSession,
        selection: GeckoSession.SelectionActionDelegate.Selection,
        actions: Array<out String>?,
        response: GeckoResponse<String>
    ) {
      Log.v("selectionTracker", selection.toString())
    }

}