package moe.tto.justbrowser.Delegates

import android.util.Log
import android.webkit.CookieManager
import kotlinx.android.synthetic.main.app_bar_main.*
import moe.tto.justbrowser.History.HistorySQLiteController
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R
import moe.tto.justbrowser.Utils.AdBlocker
import moe.tto.justbrowser.Utils.LinkUtils
import moe.tto.justbrowser.Utils.PreferenceUtils
import org.jetbrains.anko.defaultSharedPreferences
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup
import org.mozilla.geckoview.AllowOrDeny
import org.mozilla.geckoview.GeckoResult
import org.mozilla.geckoview.GeckoSession
import org.mozilla.geckoview.WebRequestError

class GeckoNavigationDelegateOverride(val activity: MainActivity) : GeckoSession.NavigationDelegate{

    var canGoBack = false
    var canGoForward = false
    val historySQLiteController = HistorySQLiteController(activity)
    val linkUtils = LinkUtils(activity)
    val preferenceUtils = PreferenceUtils(activity)
    val BLANK_PAGE_URL = "about:blank"
    override fun onLoadRequest(
        session: GeckoSession,
        request: GeckoSession.NavigationDelegate.LoadRequest
    ): GeckoResult<AllowOrDeny>? {
        if(request.uri.trim().contains(BLANK_PAGE_URL))
            return GeckoResult.DENY

        if(request.isRedirect && linkUtils.isOpenExternal(request.uri))
            activity.runToExternal(request.uri)

        if(activity.adBlocker.isAd(request.uri)){

            Log.v("onLoadRequest", "[is ad] ${request.uri}")
            return GeckoResult.DENY
        }else {
            Log.v("onLoadRequest", "[not ad] ${request.uri}")
            return GeckoResult.ALLOW
        }
    }

    override fun onLoadError(session: GeckoSession, uri: String?, error: WebRequestError): GeckoResult<String>? {
        return GeckoResult()
    }

    override fun onCanGoBack(session: GeckoSession, canGoBack: Boolean) {
        this.canGoBack = canGoBack
    }

    override fun onLocationChange(session: GeckoSession, url: String?) {
        Log.v("onUrlChange", "|${url}")
        if(url != null){
            activity.edittextUrlHolder.setText(url)
            activity.arrGeckoState[activity.currentIndexer].url = url
            activity.defaultSharedPreferences
                .edit()
                .putString(activity.getString(R.string.key_last_url_on_preference), url)
                .apply()
            if(activity.isGeckoViewLoaded) {
                doAsync {
                    val cookie = CookieManager.getInstance().getCookie(url)

                    val title = Jsoup
                        .connect(url)
                        .header("Cookie", cookie)
                        .get()
                        .title()
                    historySQLiteController.addHistory(title, url)
                }
            }
        }
    }

    override fun onCanGoForward(session: GeckoSession, canGoForward: Boolean) {
        this.canGoForward = canGoForward
    }

    override fun onNewSession(session: GeckoSession, uri: String): GeckoResult<GeckoSession>? {
        val isAutoOpenExrernal = preferenceUtils.getIsAutoExternal()
        if(isAutoOpenExrernal && linkUtils.isOpenExternal(uri)){
            activity.runToExternal(uri)
        }
        if(activity.isGeckoViewLoaded){
            activity.loadUrlToGeckoView(uri)
            activity.isGeckoViewLoaded = false
            doAsync {
                // add to history
                val cookie = CookieManager.getInstance().getCookie(uri)
                val title = Jsoup
                    .connect(uri)
                    .header("Cookie", cookie)
                    .get()
                    .title()
                historySQLiteController.addHistory(title, uri)
            }
        }
        Log.v("onNewSession", "${uri}")
        return null
    }


}