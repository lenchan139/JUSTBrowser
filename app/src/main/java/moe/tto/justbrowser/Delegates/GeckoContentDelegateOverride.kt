package moe.tto.justbrowser.Delegates

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.ActivityInfo
import android.util.Log
import android.widget.ArrayAdapter
import org.mozilla.geckoview.GeckoSession
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R
import moe.tto.justbrowser.Utils.ContentUtils
import moe.tto.justbrowser.Utils.LinkUtils

class GeckoContentDelegateOverride(val activity: MainActivity) : GeckoSession.ContentDelegate{

val contentUtils = ContentUtils(activity)
    override fun onFirstComposite(session: GeckoSession) {
    }
    override fun onContextMenu(
        session: GeckoSession,
        screenX: Int,
        screenY: Int,
        element: GeckoSession.ContentDelegate.ContextElement
    ) {
        val elementType = element.type
        val elementSrc = element.srcUri
        val uri = element.linkUri


        Log.v("contentTracker", String.format("uri: %s | src: %s", uri, elementSrc))
        if(elementSrc == null && uri == null)
            return
        else {
            var url = ""
            if(elementSrc != null)
                url = elementSrc
            else if(uri != null)
                url = uri
            if (elementType == GeckoSession.ContentDelegate.ContextElement.TYPE_IMAGE) {
                val optionsArray = arrayOf(
                    activity.getString(R.string.menu_string_save_image),
                    activity.getString(R.string.menu_string_view_image),
                    activity.getString(R.string.menu_string_copy_link)
                )
                val adapter = ArrayAdapter<String>(
                    activity,
                    android.R.layout.simple_list_item_1,
                    optionsArray
                )
                AlertDialog.Builder(activity)
                    .setTitle(url)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                        if (i == 0) {
                            contentUtils.downloadFileFromUrl(activity, url, null)
                        } else if (i == 1) {
                            activity.getCurrentSession().loadUri(url)
                        } else if (i == 2) {
                            contentUtils.copyToClipboard(url)
                        }
                    }).create().show()

            } else if (elementType == GeckoSession.ContentDelegate.ContextElement.TYPE_AUDIO) {
                val optionsArray = arrayOf(
                    activity.getString(R.string.menu_string_save_audio),
                    activity.getString(R.string.menu_string_view_audio),
                    activity.getString(R.string.menu_string_copy_link)
                )
                val adapter = ArrayAdapter<String>(
                    activity,
                    android.R.layout.simple_list_item_1,
                    optionsArray
                )
                AlertDialog.Builder(activity)
                    .setTitle(url)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                        if (i == 0) {
                            contentUtils.downloadFileFromUrl(activity, url, null)
                        } else if (i == 1) {
                            activity.getCurrentSession().loadUri(url)
                        } else if (i == 2) {
                            contentUtils.copyToClipboard(url)
                        }
                    }).create().show()
            } else if (elementType == GeckoSession.ContentDelegate.ContextElement.TYPE_VIDEO) {

                val optionsArray = arrayOf(
                    activity.getString(R.string.menu_string_save_video),
                    activity.getString(R.string.menu_string_view_video),
                    activity.getString(R.string.menu_string_copy_link)
                )
                val adapter = ArrayAdapter<String>(
                    activity,
                    android.R.layout.simple_list_item_1,
                    optionsArray
                )
                AlertDialog.Builder(activity)
                    .setTitle(url)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                        if (i == 0) {
                            contentUtils.downloadFileFromUrl(activity, url, null)
                        } else if (i == 1) {
                            activity.getCurrentSession().loadUri(url)
                        } else if (i == 2) {
                            contentUtils.copyToClipboard(url)
                        }
                    }).create().show()
            } else if (elementType == GeckoSession.ContentDelegate.ContextElement.TYPE_NONE) {

                val optionsArray = arrayOf(
                    activity.getString(R.string.menu_string_view_link),
                    activity.getString(R.string.menu_string_copy_link)
                )
                val adapter = ArrayAdapter<String>(
                    activity,
                    android.R.layout.simple_list_item_1,
                    optionsArray
                )
                if(uri != null)
                AlertDialog.Builder(activity)
                    .setTitle(url)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                        if (i == 0) {
                            activity.getCurrentSession().loadUri(url)
                        } else if (i == 1) {
                            contentUtils.copyToClipboard(url)
                        }
                    }).create().show()
            }
        }
    }

    override fun onCrash(session: GeckoSession) {

    }

    override fun onFullScreen(session: GeckoSession, fullScreen: Boolean) {
        if(fullScreen){
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR)
        }else{
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER)
        }
    }

    override fun onExternalResponse(session: GeckoSession, response: GeckoSession.WebResponseInfo) {
        Log.v("contentTracker", String.format("Uri: %s | filename: %s | contentType: %s"
            , response?.uri, response?.filename, response?.contentType))
        if(response?.uri != null &&
            (response.uri.startsWith("http") || response.uri.startsWith("ftp")) ) {
            contentUtils.downloadFileFromUrl(activity, response.uri, null )
        }else{
            LinkUtils(activity).activity
        }
    }

    override fun onCloseRequest(session: GeckoSession) {

    }

    override fun onTitleChange(session: GeckoSession, title: String?) {
        activity.lastTitle = title  ?: ""
        activity.arrGeckoState[activity.currentIndexer].title = title ?: ""
    }

    override fun onFocusRequest(session: GeckoSession) {

    }

}