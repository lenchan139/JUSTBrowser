package moe.tto.justbrowser.Delegates

import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.mozilla.geckoview.GeckoSession
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R
import moe.tto.justbrowser.Utils.LinkUtils
import moe.tto.justbrowser.Utils.PreferenceUtils
import org.jetbrains.anko.doAsync
import org.jsoup.Jsoup

class GeckoProgressDelegatesOverride(val activity: MainActivity) : GeckoSession.ProgressDelegate {
    val historySQLiteController = moe.tto.justbrowser.History.HistorySQLiteController(activity)
    val linkUtils = LinkUtils(activity)
    val preferenceUtils = PreferenceUtils(activity)
    override fun onPageStop(session: GeckoSession, success: Boolean) {
        activity.progressBar.visibility = View.GONE
        activity.refresh_geckoview.isRefreshing = false
        if(success){
            Log.v("onPageDone", "success")
            val url = activity.edittextUrlHolder.text.toString()
            val title = activity.lastTitle
            //update last title and url
            activity.arrGeckoState[activity.currentIndexer].url = url
            activity.arrGeckoState[activity.currentIndexer].title = title

            // add History
            historySQLiteController.addHistory(title, url)

            //save state
            activity.saveCurrentState {  }
            activity.isGeckoViewLoaded = true
            //check if can open external
            linkUtils.tryOpenExternal(activity, url)
            preferenceUtils.setLastBrowseUrl(url)
        }else {
            Log.v("onPageDone", "failed")
        }
    }

    override fun onSecurityChange(
        session: GeckoSession,
        securityInfo: GeckoSession.ProgressDelegate.SecurityInformation
    ) {
    }

    override fun onPageStart(session: GeckoSession, url: String) {
        Log.v("urlOnPageStart", url)
        activity.isGeckoViewLoaded = false
        activity.decideOpenExternalButton(null)
        activity.progressBar.visibility = View.VISIBLE
        activity.refresh_geckoview.isRefreshing = true
        activity.edittextUrlHolder.setText(url)
    }

    override fun onProgressChange(session: GeckoSession, progress: Int) {
        activity.progressBar.progress = progress
    }
}