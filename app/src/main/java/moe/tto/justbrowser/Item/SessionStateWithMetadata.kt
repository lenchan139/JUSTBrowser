package moe.tto.justbrowser.Item

import android.os.Parcelable
import com.evernote.android.state.State
import kotlinx.android.parcel.Parcelize
import org.mozilla.geckoview.GeckoSession

@Parcelize
data class SessionStateWithMetadata(@State var state:GeckoSession.SessionState?, @State var title:String, @State var url:String):Parcelable{

}