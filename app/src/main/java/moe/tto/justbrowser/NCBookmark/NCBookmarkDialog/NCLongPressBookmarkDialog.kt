package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.*

class NCLongPressBookmarkDialog{
    companion object {
        fun create(activity: MainActivity, ncbookmarkController:NCBookmarkMainController, item:FloccusListviewItem): AlertDialog {
            if(item.type == FloccusListviewItemConst.ITEM_TYPE_BOOKMARK){
                val arr = arrayOf("Open Bookmark", "Edit Bookmark", "Remove Bookmark")
                val adapter = ArrayAdapter(
                    activity,
                    android.R.layout.simple_list_item_1,
                    arr
                )
                return AlertDialog.Builder(activity)
                    .setTitle(item.title)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                        if(i == 0){
                            activity.drawer_layout.closeDrawers()
                            activity.loadUrlToGeckoView(item.bookmarkItem?.url)
                        }else if(i == 1){
                            val dialog = NCEditBookmarkDialog.create(activity, ncbookmarkController,
                                ncbookmarkController.ncUrl,
                                ncbookmarkController.username,
                                ncbookmarkController.password,
                                item.bookmarkItem!!
                                )
                            activity.showUniqueDialog(dialog)
                        }else if(i == 2){
                            val dialog = NCRemoveBookmarkDialog.create(
                                activity,
                                ncbookmarkController,
                                item.bookmarkItem!!
                            )
                            activity.showUniqueDialog(dialog)
                        }
                    }).create()
            }else if(item.type == FloccusListviewItemConst.ITEM_TYPE_FOLDER){
                val arr = arrayOf("Open Folder", "Edit Folder", "Remove Folder")
                val adapter = ArrayAdapter(
                    activity,
                    android.R.layout.simple_list_item_1,
                    arr
                )
                return AlertDialog.Builder(activity)
                    .setTitle(item.title)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                        if(i == 0){
                            activity.drawer_layout.closeDrawers()
                            ncbookmarkController.currentFolderId = item.uri
                        }else if(i == 1){
                            val dialog = NCEditFolderDialog.create(
                                activity,
                                ncbookmarkController,
                                item.uri, {dialog, needRefresh->
                                    if(needRefresh)
                                        ncbookmarkController.refreshList()

                                })

                            activity.showUniqueDialog(dialog)
                        }else if(i == 2){
                            NCRemoveFolderDialog.create(activity, ncbookmarkController, item, {dialog->
                                activity.showUniqueDialog(dialog)
                            })
                        }
                    }).create()
            }else{
                val dialog = AlertDialog.Builder(activity)
                .create()
                dialog.setOnShowListener {
                    it.dismiss()
                }
                return dialog
            }
        }

    }
}