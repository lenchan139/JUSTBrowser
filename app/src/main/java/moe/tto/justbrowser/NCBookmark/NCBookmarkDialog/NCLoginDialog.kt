package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_ncbookmark_login.view.*
import moe.tto.justbrowser.MainActivity
import android.view.View
import android.widget.ArrayAdapter
import moe.tto.justbrowser.R


class NCLoginDialog{
    companion object {
        fun create(activity: MainActivity, positiveCallback: ((ncUrl:String, username:String, password:String)->Unit)):AlertDialog{
            // Get the layout inflater
            val inflater = activity.getLayoutInflater()
            val dialogView = inflater.inflate(moe.tto.justbrowser.R.layout.dialog_ncbookmark_login, null)
            val edtUsername = dialogView.nc_login_username
            val edtPassword = dialogView.nc_login_password
            val edtUrl = dialogView.nc_login_url
            val urlSpinner = dialogView.urlSpinner
            val dialog = AlertDialog
                .Builder(activity)
                .setCancelable(true)
                .setView(dialogView)
                .setTitle("Login NC Bookmark Viewer")
                .setOnCancelListener {
                    it.dismiss()
                }.setPositiveButton("Login", null)
                .setNegativeButton("Cancel", null)
                .create()

            val spinnerAdapter = ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, activity.resources.getStringArray(R.array.spinner_array_url_header));
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            urlSpinner.adapter = spinnerAdapter
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        val url =
                            String.format("%s%s",
                                activity.resources.getStringArray(R.array.spinner_array_url_header)[urlSpinner.selectedItemPosition],
                                edtUrl.text.toString())
                        if(url.last() == '/') url.substring(0, url.length-2)
                        positiveCallback(url, edtUsername.text.toString(), edtPassword.text.toString())
                        val wantToCloseDialog = false
                        //Do stuff, possibly set wantToCloseDialog to true then...
                        if (wantToCloseDialog)
                            dialog.dismiss()
                        //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                    }
                })
            }
            return dialog
            //
        }
    }
}