package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_add_folder.view.*
import kotlinx.coroutines.selects.select
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.NCBookmarkMainController
import moe.tto.justbrowser.NCBookmark.NCBookmarkUtils
import moe.tto.justbrowser.NCBookmark.NCFolderItem

class NCNewFolderDialog{
    companion object {
        fun create(activity: MainActivity,
                   ncBookmarkMainController: NCBookmarkMainController,
                   folders:ArrayList<NCFolderItem>,
                   onRefreshCallback:(needRefresh:Boolean)->Unit):AlertDialog{
            val inflater = activity.getLayoutInflater()
            val dialogView = inflater.inflate(moe.tto.justbrowser.R.layout.dialog_add_folder, null)
            val edtNewFolderName = dialogView.nc_add_folder_name
            val edtParentFoler = dialogView.nc_add_folder_parent
            var selectDialog : AlertDialog? = null
            var currentFolderId = "-1"
            val dialog = AlertDialog.Builder(activity)
                .setTitle("New Folder")
                .setView(dialogView)
                .setPositiveButton("Add", null)
                .setNegativeButton("Cancel", null)
                .setOnCancelListener {
                    onRefreshCallback(false)
                }
                .create()
            edtNewFolderName.requestFocus()
            activity.showKeyboard()
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isClickable = false
                    NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
                        .addFolder(ncBookmarkMainController.ncUrl,
                            ncBookmarkMainController.username,
                            ncBookmarkMainController.password,
                            edtNewFolderName.text.toString(),
                            currentFolderId,
                            {isSuccess, error ->
                                if(isSuccess){
                                    dialog.dismiss()
                                    onRefreshCallback(true)
                                    Toast.makeText(activity, "Folder added!", Toast.LENGTH_SHORT).show()
                                }else{
                                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isClickable = true
                                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                                }

                            })
                }
            }
            edtParentFoler.setOnClickListener {
                val dialog = NCSelectFolderDialog.create(
                    activity,ncBookmarkMainController, folders, true, {folderItem->
                        currentFolderId = folderItem.id
                        edtParentFoler.setText(folderItem.title)
                    },{})
                if(selectDialog!=null && selectDialog!!.isShowing)
                    selectDialog!!.dismiss()
                selectDialog = dialog
                selectDialog!!.show()
            }
            return dialog
        }
    }
}