package moe.tto.justbrowser.NCBookmark

import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_full_body.*
import kotlinx.android.synthetic.main.nav_header_main.*
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.NCBookmarkDialog.NCAddBookmarkDialog
import moe.tto.justbrowser.NCBookmark.NCBookmarkDialog.NCLoginDialog
import moe.tto.justbrowser.NCBookmark.NCBookmarkDialog.NCLogoutDialog
import moe.tto.justbrowser.NCBookmark.NCBookmarkDialog.NCLongPressBookmarkDialog
import moe.tto.justbrowser.R
import moe.tto.justbrowser.Utils.PreferenceUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class NCBookmarkMainController(val activity: MainActivity){
    val preferenceUtils = PreferenceUtils(activity)
    var ncBookmarkUtils : NCBookmarkUtils? = null
    var ncUrl = preferenceUtils.getNCLoginUrl()
    var username = preferenceUtils.getNCLoginUsername()
    var password = preferenceUtils.getNCLoginPassword()
    val listview = activity.nav_body_listview
    val btnLogin = activity.nav_body_button_login
    val btnLogout = activity.nav_body_button_logout
    val txtStatus = activity.nav_body_status
    val headrSubtitle = activity.nav_header_subtitle
    val pulldownlayout = activity.nav_body_refresh_list
    val btnAddBookmark = activity.nav_body_button_addBookmark
    val statusReplacer = activity.getString(R.string.ncbookmark_status_prefix)
    var currentFolderId = "-1"
    init{
        initUi()
        checkUserFirst()
    }
    fun checkUserFirst(){
        if(ncUrl.isNotEmpty() && username.isNotEmpty() && password.isNotEmpty()){
            ncBookmarkUtils = NCBookmarkUtils(activity, ncUrl)
            ncBookmarkUtils?.testConnection(username, password,{isSuccess, error ->
                if(isSuccess) {
                    ncBookmarkUtils = NCBookmarkUtils(activity, ncUrl)
                    uiUpdateOnLogin()
                    getFolders(currentFolderId)

                }
            })
        }else{
            uiUpdateOnLogout()
        }
    }
    fun initUi(){
        listview.adapter = null
        btnLogin.setOnClickListener {
            val dialog = NCLoginDialog.create(activity, { ncUrl, username, password ->
                ncBookmarkUtils = NCBookmarkUtils(activity, ncUrl)
                ncBookmarkUtils?.testConnection(username, password, { isSuccess, error ->
                    if(isSuccess){
                        activity.uniqueDialog?.dismiss()
                        this.username = username
                        this.password = password
                        this.ncUrl = ncUrl
                        preferenceUtils.setNCLoginUrl(ncUrl)
                        preferenceUtils.setNCLoginUsername(username)
                        preferenceUtils.setNCLoginPassword(password)
                        uiUpdateOnLogin()
                        getFolders(currentFolderId)
                    }else{
                        uiUpdateOnLogout()
                        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                    }
                })
            })
            activity.showUniqueDialog(dialog)
        }
        btnLogout.setOnClickListener {
            val dialog = NCLogoutDialog.create(activity, {
                preferenceUtils.cleanNCLoginUsernamePassswordUrl()
                ncBookmarkUtils = null
                uiUpdateOnLogout()
            })
            activity.showUniqueDialog(dialog)
        }
        initPullDownRefresh()
        btnAddBookmark.setOnClickListener {
            val bookmark = NCBookmarkItem(
                -1,
                activity.getCurrentTitle(),
                activity.getCurrentUrl(),
                currentFolderId
            )
            val dialog = NCAddBookmarkDialog.create(activity, this, ncUrl, username, password, bookmark)
            activity.showUniqueDialog(dialog)
        }
    }
    fun refreshList(){
        getFolders(currentFolderId)
    }
    fun getFolders(folderId:String){
        pulldownlayout.isRefreshing = true
        txtStatus.text = String.format(statusReplacer, activity.getString(R.string.ncbookmark_status_fetching_folders))
        ncBookmarkUtils?.fetchFolderList(username, password, {result->
            ncBookmarkUtils?.updateFolders(activity, result, {ncFolderHelper ->
                if(ncFolderHelper  != null) {
                    val folders = ncBookmarkUtils?.getFoldersFromDatabase(ncFolderHelper, folderId)
                    val currentFolder = ncFolderHelper.getCurrentFolderInfo(folderId)
                    if(folders != null)
                            getBookmarks(folders, currentFolder)
                }else{

                }
            })
        })
    }
    fun getBookmarks(folders:ArrayList<FloccusListviewItem>, currentFolder:NCFolderItem){
        txtStatus.text = String.format(statusReplacer, activity.getString(R.string.ncbookmark_status_fetching_bookmarks))
        ncBookmarkUtils?.getBookmarks(currentFolder.id, username, password, {bookmarks ->
            if(bookmarks != null)
                doAsync {
                    uiThread {
                        updateListView(folders, bookmarks, currentFolder)
                    }
                }

        })
    }
    fun updateListView(folders:ArrayList<FloccusListviewItem>, bookmarks:ArrayList<FloccusListviewItem>, currentFolder:NCFolderItem){
        txtStatus.text = String.format(statusReplacer, activity.getString(R.string.ncbookmark_status_updating_display_list_dotdotdot))
        val finalList = ArrayList<FloccusListviewItem>()
        finalList.addAll(folders)
        finalList.addAll(bookmarks)
        val adapter = object : ArrayAdapter<FloccusListviewItem>(
            activity,
            android.R.layout.simple_list_item_1,
            finalList){

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

                val view = super.getView(position, convertView, parent)
                val txt = view.findViewById<TextView>(android.R.id.text1)
                val currentItem = finalList[position]
                txt.setText("${currentItem.prefixEmoji}${currentItem.title}")
                return view
            }
        }
        listview.adapter = adapter
        listview.setOnItemClickListener { adapterView, view, i, l ->
            val item = finalList[i]
            if(item.type == FloccusListviewItemConst.ITEM_TYPE_BACK_CONTROLL){
                currentFolderId = currentFolder.parend_id
                getFolders(currentFolderId)
            }else if(item.type == FloccusListviewItemConst.ITEM_TYPE_FOLDER){
                currentFolderId = item.uri
                getFolders(item.uri)
            }else if(item.type == FloccusListviewItemConst.ITEM_TYPE_BOOKMARK){
                activity.loadUrlToGeckoView(item.uri)
                activity.drawer_layout.closeDrawers()
            }
        }
        listview.setOnItemLongClickListener { adapterView, view, i, l ->
            activity.showUniqueDialog(NCLongPressBookmarkDialog.create(activity, this, finalList[i]))
            true
        }
        listview.deferNotifyDataSetChanged()
        txtStatus.text = String.format(statusReplacer, activity.getString(R.string.ncbookmark_status_fetch_completed))
        pulldownlayout.isRefreshing = false
    }
    fun uiUpdateOnLogout(){
        btnLogin.visibility = View.VISIBLE
        btnLogout.visibility = View.GONE
        listview.visibility = View.INVISIBLE
        btnAddBookmark.visibility = View.GONE
        listview.adapter = null
        listview.deferNotifyDataSetChanged()
        pulldownlayout.isRefreshing = false
        headrSubtitle.text = activity.getString(R.string.nav_header_subtitle)
        txtStatus.text = String.format(statusReplacer, activity.getString(R.string.ncbookmark_status_logged_out_dot))
    }
    fun uiUpdateOnLogin(){
        btnLogin.visibility = View.GONE
        btnLogout.visibility = View.VISIBLE
        listview.visibility = View.VISIBLE
        btnAddBookmark.visibility = View.VISIBLE
        pulldownlayout.isRefreshing = false
        var shorNcUrl = ncUrl
        for (str in activity.resources.getStringArray(R.array.spinner_array_url_header)){
            shorNcUrl = shorNcUrl.replace(str, "")
        }
        headrSubtitle.text = String.format("%s@%s",  username, shorNcUrl)
        txtStatus.text = String.format(statusReplacer, activity.getString(R.string.ncbookmark_status_midfix_login_as_) + username)

    }
    fun initPullDownRefresh(){
        pulldownlayout.setOnRefreshListener {
            getFolders(currentFolderId)
        }
        pulldownlayout.setColorSchemeColors(
            activity.resources.getColor(android.R.color.holo_blue_bright),
            activity.resources.getColor(android.R.color.holo_green_light),
            activity.resources.getColor(android.R.color.holo_orange_light),
            activity.resources.getColor(android.R.color.holo_red_light)
        );

    }
}