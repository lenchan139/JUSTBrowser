package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_add_change_ncbookmark.view.*
import kotlinx.android.synthetic.main.nav_full_body.*
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.*

class NCAddBookmarkDialog{
    companion object {
        private val folderSpacePrefix = "　"
        private val folderArrowPrefix = "⤷"
        private fun getSubFolders(activity: MainActivity, folderHelper: NCFolderHelper, folderId:String, prefix:String, preFolders:ArrayList<NCFolderItem>):ArrayList<NCFolderItem>{
            val returnFolders = ArrayList<NCFolderItem>()
            for(i in 0..preFolders.size-1){
                val o = preFolders[i]
                o.title = prefix + folderArrowPrefix + o.title
                returnFolders.add(o)
                val nextFolders = folderHelper.getDisplayList(o.id)
                if(nextFolders.isNotEmpty()) {
                    val moreFolders = getSubFolders(activity, folderHelper, o.id, folderSpacePrefix + prefix, nextFolders)
                    for(s in moreFolders){
                        returnFolders.add(s)
                    }
                }
            }
            return returnFolders

        }

        fun create(
            activity: MainActivity,
            ncbookmarkController: NCBookmarkMainController,
            ncUrl:String,
            username:String ,
            password:String,
            bookmark: NCBookmarkItem?): AlertDialog {
            val inflater = activity.getLayoutInflater()
            val dialogView = inflater.inflate(moe.tto.justbrowser.R.layout.dialog_add_change_ncbookmark, null)
            val edtUrl = dialogView.nc_add_change_url
            val edtTitle = dialogView.nc_add_change_title
            val edtFolder = dialogView.nc_add_change_folder
            val edtTags = dialogView.nc_add_change_tags
            val edtDesc = dialogView.nc_add_change_desc
            var folderDialog : AlertDialog? = null
            var currentFolderId = "-1"
            var folders = ArrayList<NCFolderItem>()
            fun refreshFolders(callback:()->Unit){
                NCBookmarkUtils(activity, ncUrl)
                    .fetchFolderList(username, password, {result->
                        val ncFolderHelper = NCFolderHelper(activity, result)
                        val rootFolder = ncFolderHelper.getDisplayList("-1")
                        val currFolder = ncFolderHelper.getCurrentFolderInfo(currentFolderId)
                        edtFolder.setText(currFolder.title)
                        folders.clear()
                        folders.add(NCFolderItem("-1", "", "!ROOT"))
                        folders.addAll(getSubFolders(activity, ncFolderHelper, "-1", "", rootFolder))
                        callback()
                    })
            }
            refreshFolders({
            })

            NCBookmarkUtils(activity, ncUrl)
                .getFolder(ncbookmarkController.currentFolderId, username, password, {
                    Log.v("yo", it.toString())
                    edtTitle.requestFocus()
                    activity.showKeyboard()

                    if(it!=null)
                        edtFolder.setText(it.title)
                })
            val dialog = AlertDialog
                .Builder(activity)
                .setCancelable(true)
                .setView(dialogView)
                .setTitle("Add Bookmark")
                .setOnCancelListener {
                    it.dismiss()
                }.setPositiveButton("Add", null)
                .setNegativeButton("Cancel", null)
                .create()
            if((bookmark?.title?:"").isNotEmpty())
                edtTitle.setText(bookmark?.title)
            if((bookmark?.url?:"").isNotEmpty())
                edtUrl.setText(bookmark?.url)
            if((bookmark?.folderId?:"").isNotEmpty())
                currentFolderId = bookmark?.folderId?:currentFolderId
            dialog.setOnShowListener{
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        if(edtUrl.text.isNotEmpty() && edtTitle.text.isNotEmpty()){
                            val tampTag = NCTagUtils.strToArray(edtTags.text.toString())
                            val title = edtTitle.text.toString()
                            val url = edtUrl.text.toString()
                            val desc = edtDesc.text.toString()
                            val ncBookmarkUtils = NCBookmarkUtils(activity, ncUrl)
                            ncBookmarkUtils.testConnection(username, password, {isSuccess, error ->
                                if(isSuccess){
                                    ncBookmarkUtils.addBookmark(
                                        username= username,
                                        password = password,
                                        ncUrl = ncUrl,
                                        url = url,
                                        desc = desc,
                                        tags = tampTag,
                                        title = title,
                                        folderId = currentFolderId,
                                        callback = {isSuccess, error ->
                                            if(isSuccess){
                                                dialog.dismiss()
                                                ncbookmarkController.getFolders(ncbookmarkController.currentFolderId)
                                                Toast.makeText(activity, "Bookmark added!", Toast.LENGTH_SHORT).show()
                                            }else{
                                                Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                                            }
                                        })
                                }else{
                                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                                }

                            })
                        }else{
                            Toast.makeText(activity, "Title and URL must not empty!", Toast.LENGTH_SHORT).show()
                        }




                        val wantToCloseDialog = false
                        //Do stuff, possibly set wantToCloseDialog to true then...
                        if (wantToCloseDialog)
                            dialog.dismiss()
                        //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                    }
                })
            }
            fun openSelectFolderDialog(){
                val dialog = NCSelectFolderDialog.create(activity,ncbookmarkController, folders,
                    false, { folder->
                    edtFolder.setText(folder.title)
                    currentFolderId = folder.id
                },{isSuccess ->
                    val dialog = NCNewFolderDialog.create(
                        activity,
                        ncbookmarkController,
                        folders,
                        {
                            if(it){
                                refreshFolders({
                                    openSelectFolderDialog()
                                })
                            }else{
                                openSelectFolderDialog()
                            }
                        })
                    dialog.show()
                })

                if(folderDialog!=null && folderDialog!!.isShowing)
                    folderDialog!!.dismiss()
                folderDialog = dialog
                folderDialog?.show()
            }
            edtFolder.setOnClickListener {
                openSelectFolderDialog()
            }

            // adter dialog onclick setter

            return dialog

        }
    }
}