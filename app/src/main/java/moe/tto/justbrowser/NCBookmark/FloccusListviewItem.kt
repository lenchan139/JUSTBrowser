package moe.tto.justbrowser.NCBookmark


class FloccusListviewItem(var type:Int, var title:String, var uri:String, var bookmarkItem:NCBookmarkItem?){
    val prefixEmoji : String
        get(){
            if(type == FloccusListviewItemConst.ITEM_TYPE_FOLDER)
                return "\uD83D\uDCC1 "
            else if(type == FloccusListviewItemConst.ITEM_TYPE_BOOKMARK)
                return "\uD83D\uDD16 "
            else
                return "  "
        }
}
class FloccusListviewItemConst{
    companion object {
        val ITEM_TYPE_BACK_CONTROLL: Int = -1
        val ITEM_TYPE_FOLDER: Int = 0
        val ITEM_TYPE_BOOKMARK: Int = 1
    }

}
