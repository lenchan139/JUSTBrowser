package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_add_folder.view.*
import kotlinx.coroutines.selects.select
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.*

class NCEditFolderDialog{
    companion object {
        private val folderSpacePrefix = "　"
        private val folderArrowPrefix = "⤷"
        private fun getSubFolders(activity: MainActivity, folderHelper: NCFolderHelper, folderId:String, prefix:String, preFolders:ArrayList<NCFolderItem>):ArrayList<NCFolderItem>{
            val returnFolders = ArrayList<NCFolderItem>()
            for(i in 0..preFolders.size-1){
                val o = preFolders[i]
                o.title = prefix + folderArrowPrefix + o.title
                returnFolders.add(o)
                val nextFolders = folderHelper.getDisplayList(o.id)
                if(nextFolders.isNotEmpty()) {
                    val moreFolders = getSubFolders(activity, folderHelper, o.id, folderSpacePrefix + prefix, nextFolders)
                    for(s in moreFolders){
                        returnFolders.add(s)
                    }
                }
            }
            return returnFolders

        }
        fun create(activity: MainActivity,
                   ncBookmarkMainController: NCBookmarkMainController,
                   folderId:String,
                   onRefreshCallback:(dialog: AlertDialog, needRefresh:Boolean)->Unit):AlertDialog{
            val inflater = activity.getLayoutInflater()
            val dialogView = inflater.inflate(moe.tto.justbrowser.R.layout.dialog_add_folder, null)
            val edtNewFolderName = dialogView.nc_add_folder_name
            val edtParentFoler = dialogView.nc_add_folder_parent
            var selectDialog : AlertDialog? = null

            edtNewFolderName.isClickable = false
            edtParentFoler.isClickable = false
            var currentFolderId = if(folderId.isNotEmpty()) folderId else "-1"
            var parentFolderId :String = "-1"
            var folders = ArrayList<NCFolderItem>()
            fun refreshFolders(callback:()->Unit){
                NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
                    .fetchFolderList(ncBookmarkMainController.username, ncBookmarkMainController.password, {result->
                        val ncFolderHelper = NCFolderHelper(activity, result)
                        val rootFolder = ncFolderHelper.getDisplayList("-1")
                        val currFolder = ncFolderHelper.getCurrentFolderInfo(currentFolderId)
                        val parentFolder = ncFolderHelper.getCurrentFolderInfo(currFolder.parend_id)
                        edtNewFolderName.setText(currFolder.title)
                        edtParentFoler.setText(
                            if(parentFolder.id.isNotEmpty() && parentFolder.id!="-1") {
                                parentFolder.title
                            }
                            else
                                "!ROOT")
                        folders.clear()
                        folders.add(NCFolderItem("-1", "", "!ROOT"))
                        folders.addAll(
                            this.getSubFolders(
                                activity,
                                ncFolderHelper,
                                "-1",
                                "",
                                rootFolder
                            )
                        )
                        callback()
                    })
            }
            refreshFolders({})

            NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
                .getFolder(currentFolderId, ncBookmarkMainController.username, ncBookmarkMainController.password, {it1->
                    Log.v("yo", it1.toString())
                    if(it1!=null) {
                        NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
                            .getFolder(it1.parend_id, ncBookmarkMainController.username, ncBookmarkMainController.password, {it2->
                                Log.v("yo", it2.toString())
                                if(it2!=null) {
                                    edtParentFoler.setText(it2.title)
                                    parentFolderId = it2.id
                                }

                                edtNewFolderName.setText(it1.title)
                                edtNewFolderName.isClickable = true
                                edtParentFoler.isClickable = true
                                edtNewFolderName.requestFocus()
                                activity.showKeyboard()
                            })
                    }
                })
            val dialog = AlertDialog.Builder(activity)
                .setTitle("Edit Folder")
                .setView(dialogView)
                .setPositiveButton("Change", null)
                .setNegativeButton("Cancel", null)

                .create()
            dialog.setOnCancelListener {
                onRefreshCallback(dialog, false)
            }
            dialog.setOnShowListener {dialogInterface->
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isClickable = false
                    NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
                        .editFolder(ncBookmarkMainController.ncUrl,
                            ncBookmarkMainController.username,
                            ncBookmarkMainController.password,
                            currentFolderId,
                            edtNewFolderName.text.toString(),
                            parentFolderId,
                            {isSuccess, error ->
                                if(isSuccess){
                                    dialog.dismiss()
                                    onRefreshCallback(dialog, true)
                                    Toast.makeText(activity, "Folder added!", Toast.LENGTH_SHORT).show()
                                }else{
                                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).isClickable = true
                                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                                }

                            })
                }
            }
            dialog.setOnDismissListener {
                edtNewFolderName.clearFocus()
                activity.hideKeyboard()
            }
            edtParentFoler.setOnClickListener {
                val dialog = NCSelectFolderDialog.create(
                    activity,ncBookmarkMainController, folders, true, {folderItem->
                        parentFolderId = folderItem.id
                        edtParentFoler.setText(folderItem.title)
                    },{})
                if(selectDialog!=null && selectDialog!!.isShowing)
                    selectDialog!!.dismiss()
                selectDialog = dialog
                selectDialog?.show()
            }
            return dialog
        }
    }
}