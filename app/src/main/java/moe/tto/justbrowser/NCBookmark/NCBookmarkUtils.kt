package moe.tto.justbrowser.NCBookmark

import android.content.Context
import android.util.Base64
import android.util.Log
import moe.tto.justbrowser.R
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Connection
import org.jsoup.HttpStatusException
import java.io.FileNotFoundException
import java.io.IOException
import java.net.UnknownHostException
import kotlin.coroutines.coroutineContext

class NCBookmarkUtils(val context:Context, val ncUrl:String){
    fun toBase64Login(username:String, password:String): String {
        val login = "${username}:${password}"
        val base64login = String(Base64.encode(login.toByteArray(), 0))
        return base64login
    }
    fun testConnection(username: String, password:String, callback:((isSuccess: Boolean, error:String)->Unit)){
        var isConnected = false
        var isValid = false
        var errorMessage : String = ""
        val login = toBase64Login(username, password)
        val dlUrl = "${ncUrl}${NCBookmarkConstant.BOOKMARK_ENDPOINT}"
        var result = ""
        Log.v("testUrl", dlUrl)
        doAsync {
            try {
                val r = NCBookmarkConstant.getDefaultJsoup(Connection.Method.GET, dlUrl, login)
                    .execute().parse()
                isConnected = true
                if(r != null){
                    result = r.body().text()

                    val status = JSONObject(result).get("status") as String
                    if(status.equals("success")) {
                        isValid = true
                    }else{
                        throw JSONException("invalid status")
                    }
                }
                Log.v("JSON_FOR_VALIDATE_ACC",r.toString())
                //val json = JSON

                isConnected = true
            }catch (e: HttpStatusException) {
                e.printStackTrace()
                errorMessage = context.getString(R.string.ncbookmark_login_errorMessage_url_uname_pass_invalid)
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
                isConnected = false
                errorMessage = context.getString(R.string.ncbookmark_login_errorMessage_incorrect_url)
            }catch (e: UnknownHostException){
                e.printStackTrace()
                isConnected = false
                errorMessage = context.getString(R.string.ncbookmark_login_errorMessage_unknow_host)
            }catch (e: FileNotFoundException){
                e.printStackTrace()
                isConnected = true
                errorMessage = context.getString(R.string.ncbookmark_login_errorMessage_file_not_found)
            } catch (e: IOException) {
                e.printStackTrace()
                isConnected = false
                errorMessage = context.getString(R.string.ncbookmark_login_errorMessage_connection_error)
            }catch (e:JSONException){
                e.printStackTrace()
                isValid = false
                errorMessage = context.getString(R.string.ncbookmark_login_errorMessage_auth_failed)
            }


            if(dlUrl.length > 0 && username.length > 0 && password.length > 0 && isConnected && isValid) {
                uiThread {  callback(true, errorMessage) }
            }else{
                uiThread{ callback(false, errorMessage) }
            }
        }
    }

    fun fetchFolderList(username:String, password:String , callback: ((String)->Unit)){
        var result: String = ""

        val base64login = toBase64Login(username, password)
        doAsync {
            val dlUrl = "${ncUrl}${NCBookmarkConstant.FOLDER_ENDPOINT}"
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = NCBookmarkConstant.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                    .execute().parse().body().text()
            } catch (e: IOException) {
                e.printStackTrace()
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
            }
            Log.v("v2floccus folder json", result)
            uiThread {
                callback(result)
            }
        }
    }
    fun updateFolders(context: Context, folderJsonString:String?, callback: (NCFolderHelper?) -> Unit){
        if(folderJsonString == null)
            return
        try {
            val json = JSONObject(folderJsonString)
            if(json["status"] == "success"){
                val ncFolderHelper = NCFolderHelper(context, folderJsonString)
                callback(ncFolderHelper)
            }else{
                callback(null)
            }


        }catch (e:Exception){
            e.printStackTrace()
            callback(null)
        }

    }

    fun getFoldersFromDatabase(folderHelper:NCFolderHelper, folderId: String): ArrayList<FloccusListviewItem> {
        val list = folderHelper.getDisplayList(folderId)
        val arrListItems = ArrayList<FloccusListviewItem>()
        if(!folderId.equals("-1")){
            val currFolder = folderHelper.getCurrentFolderInfo(folderId)
            arrListItems.add(FloccusListviewItem(FloccusListviewItemConst.ITEM_TYPE_FOLDER, "..", currFolder.parend_id, null))
        }

        Log.v("v2floccus parent size", list.size.toString())
        for(i in list){
            Log.v("v2floccus parent", i.title)
            arrListItems.add(FloccusListviewItem(FloccusListviewItemConst.ITEM_TYPE_FOLDER, i.title, i.id, null))

        }
        //updateListView(arrListItems)
        return arrListItems
    }


    fun getBookmarks(folderId:String, username: String, password: String, callback: (ArrayList<FloccusListviewItem>?) -> Unit){
        var result = ""
        val base64login = toBase64Login(username, password)
        doAsync {
            val dlUrl = ncUrl + NCBookmarkConstant.getBookmarkEndPonint(folderId)
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = NCBookmarkConstant.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                    .execute().parse().body().text()

                Log.v("v2floccus result body", result)
                // to array
                val jsonObj = JSONObject(result)
                if(jsonObj.has("data")){
                    val jsonArray = jsonObj.getJSONArray("data")
                    val arrayFloccusList = ArrayList<FloccusListviewItem>()
                    for(i in 0..jsonArray.length()-1){
                        val bmObj = jsonArray.getJSONObject(i)
                        val bookmark = NCBookmarkItem(
                            title = bmObj.getString("title"),
                            url = bmObj.getString("url"),
                            bookmarkId = bmObj.getString("id").toInt(),
                            folderId = bmObj.getJSONArray("folders").getString(0))
                        arrayFloccusList.add(FloccusListviewItem(
                            FloccusListviewItemConst.ITEM_TYPE_BOOKMARK,
                            bookmark.title, bookmark.url, bookmark))
                    }
                    uiThread {
                        callback(arrayFloccusList)
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }catch (e:java.lang.Exception){
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }


        }
    }

    fun getBookmark(bookmarkId: Int, username: String, password: String, callback: (JSONObject?) -> Unit){
        var result = ""
        val base64login = toBase64Login(username, password)
        doAsync {
            val dlUrl = ncUrl + NCBookmarkConstant.BOOKMARK_ENDPOINT + "/${bookmarkId}"
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = NCBookmarkConstant.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                    .execute().parse().body().text()

                Log.v("v2floccus result body", result)
                // to array
                val jsonObj = JSONObject(result)
                if(jsonObj.has("item")){
                    val bmObj = jsonObj.getJSONObject("item")

                    }
                    uiThread {
                        callback(jsonObj)
                    }

            } catch (e: IOException) {
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }catch (e:java.lang.Exception){
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }


        }
    }

    fun getFolder(bookmarkId: String, username: String, password: String, callback: (NCFolderItem?) -> Unit){
        var result = ""
        val base64login = toBase64Login(username, password)
        doAsync {
            val dlUrl = ncUrl + NCBookmarkConstant.FOLDER_ENDPOINT + "/${bookmarkId}"
            Log.v("v2floccus dl url", dlUrl)
            try {
                result = NCBookmarkConstant.getDefaultJsoup(Connection.Method.GET, dlUrl, base64login)
                    .execute().parse().body().text()

                Log.v("v2floccus result body", result)
                // to array
                val jsonObj = JSONObject(result)
                var resultFolderItem : NCFolderItem? =null
                if(jsonObj.has("item")){
                    val bmObj = jsonObj.getJSONObject("item")
                    resultFolderItem = NCFolderItem(
                        bmObj.getString("id"),
                        bmObj.getString("parent_folder"),
                                bmObj.getString("title")
                    )
                }
                uiThread {
                    callback(resultFolderItem)
                }

            } catch (e: IOException) {
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }catch(e:IllegalArgumentException){
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }catch (e:java.lang.Exception){
                e.printStackTrace()
                uiThread {
                    callback(null)
                }
            }


        }
    }
    fun addBookmark(ncUrl: String,
                    username: String,
                    password: String,
                    url: String,
                    title:String,
                    folderId: String,
                    desc: String,
                    tags:Array<String>?,
                    callback: (isSuccess: Boolean, error: String) -> Unit
                    ){
        doAsync {
            val base64login = toBase64Login(username, password)
            val jsoup = NCBookmarkConstant.getDefaultJsoup(Connection.Method.POST, ncUrl+NCBookmarkConstant.BOOKMARK_ENDPOINT, base64login)
            if(url.isNotEmpty()) jsoup.data("url", url)
            if(title.isNotEmpty()) jsoup.data("title", title)
            if(desc.isNotEmpty()) jsoup.data("description", desc)
            if(folderId.isNotEmpty()) jsoup.data("folders[]", folderId)
            if(tags != null ) {
                for (tag in tags) {
                    if (tag.trim().isNotEmpty()) {
                        jsoup.data("item[tags][]", tag.trim())
                    }
                }
            }
            try {
                val result = jsoup.execute().parse()
                uiThread {  callback(true, "") }
            }catch(e:IOException){
                e.printStackTrace()
                uiThread { callback(false, e.localizedMessage) }
            }

        }
    }

    fun editBookmark(ncUrl: String,
                    username: String,
                    password: String,
                    bookmarkId:Int,
                    url: String,
                    title:String,
                    folderId: String,
                    desc: String,
                    tags:Array<String>?,
                    callback: (isSuccess: Boolean, error: String) -> Unit
    ){
        doAsync {
            val base64login = toBase64Login(username, password)
            val dlUrl = ncUrl+NCBookmarkConstant.BOOKMARK_ENDPOINT + "/${bookmarkId}"
            val jsoup = NCBookmarkConstant.getDefaultJsoup(Connection.Method.PUT,dlUrl , base64login)
            if(bookmarkId >= 0 && bookmarkId.toString().isNotEmpty()) jsoup.data("record_id", bookmarkId.toString())
            if(url.isNotEmpty()) jsoup.data("url", url)
            if(title.isNotEmpty()) jsoup.data("title", title)
            if(desc.isNotEmpty()) jsoup.data("description", desc)
            if(folderId.isNotEmpty()) jsoup.data("folders[]", folderId)
            if(tags != null ) {
                for (tag in tags) {
                    if (tag.trim().isNotEmpty()) {
                        jsoup.data("item[tags][]", tag.trim())
                    }
                }
            }
            try {
                val result = jsoup.execute().parse()
                uiThread {  callback(true, "") }
            }catch(e:IOException){
                e.printStackTrace()
                uiThread { callback(false, e.localizedMessage) }
            }

        }
    }

    fun removeBookmark(ncUrl: String,
                     username: String,
                     password: String,
                     currFolderId:String,
                     callback: (isSuccess: Boolean, error: String) -> Unit
    ){
        doAsync {
            val base64login = toBase64Login(username, password)
            val jsoup = NCBookmarkConstant.getDefaultJsoup(Connection.Method.DELETE, ncUrl+NCBookmarkConstant.BOOKMARK_ENDPOINT+"/"+currFolderId, base64login)
            try {
                val result = jsoup.execute().parse()
                Log.v("removeBookmarkBody", result.body().text())
                uiThread {  callback(true, "") }
            }catch(e:IOException){
                e.printStackTrace()
                uiThread { callback(false, e.localizedMessage) }
            }

        }
    }
    fun addFolder(ncUrl: String,
                    username: String,
                    password: String,
                    title:String,
                    parentFolderId: String,
                    callback: (isSuccess: Boolean, error: String) -> Unit
    ){
        doAsync {
            val base64login = toBase64Login(username, password)
            val jsoup = NCBookmarkConstant.getDefaultJsoup(Connection.Method.POST, ncUrl+NCBookmarkConstant.FOLDER_ENDPOINT, base64login)
            if(title.isNotEmpty()) jsoup.data("title", title)
            if(parentFolderId.isNotEmpty()) jsoup.data("parent_folder", parentFolderId)
            try {
                val result = jsoup.execute().parse()
                uiThread {  callback(true, "") }
            }catch(e:IOException){
                e.printStackTrace()
                uiThread { callback(false, e.localizedMessage) }
            }

        }
    }

    fun editFolder(ncUrl: String,
                  username: String,
                  password: String,
                  currFolderId:String,
                  title:String,
                  parentFolderId: String,
                  callback: (isSuccess: Boolean, error: String) -> Unit
    ){
        doAsync {
            val base64login = toBase64Login(username, password)
            val jsoup = NCBookmarkConstant.getDefaultJsoup(Connection.Method.PUT, ncUrl+NCBookmarkConstant.FOLDER_ENDPOINT+"/"+currFolderId, base64login)
            if(title.isNotEmpty()) jsoup.data("title", title)
            if(parentFolderId.isNotEmpty()) jsoup.data("parent_folder", parentFolderId)
            try {
                val result = jsoup.execute().parse()
                Log.v("editFolderBody", result.body().text())
                uiThread {  callback(true, "") }
            }catch(e:IOException){
                e.printStackTrace()
                uiThread { callback(false, e.localizedMessage) }
            }

        }
    }
    fun removeFolder(ncUrl: String,
                   username: String,
                   password: String,
                   currFolderId:String,
                   callback: (isSuccess: Boolean, error: String) -> Unit
    ){
        doAsync {
            val base64login = toBase64Login(username, password)
            val jsoup = NCBookmarkConstant.getDefaultJsoup(Connection.Method.DELETE, ncUrl+NCBookmarkConstant.FOLDER_ENDPOINT+"/"+currFolderId, base64login)
            try {
                val result = jsoup.execute().parse()
                Log.v("editFolderBody", result.body().text())
                uiThread {  callback(true, "") }
            }catch(e:IOException){
                e.printStackTrace()
                uiThread { callback(false, e.localizedMessage) }
            }

        }
    }
}