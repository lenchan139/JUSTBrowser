package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import moe.tto.justbrowser.MainActivity

class NCLogoutDialog{
    companion object {
        fun create(activity: MainActivity, callback:()->Unit):AlertDialog{
            return AlertDialog.Builder(activity)
                .setTitle("Logout?")
                .setMessage("Are you sure to logout your NCBookmark account?")
                .setNegativeButton("No", null)
                .setPositiveButton("Logout", DialogInterface.OnClickListener { dialogInterface, i ->
                    callback()
                }).create()
        }
    }
}