package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.*

class NCRemoveBookmarkDialog{
    companion object {
        fun create(
            activity: MainActivity,
            ncBookmarkMainController: NCBookmarkMainController,
            bookmark: NCBookmarkItem
        ): AlertDialog {
            val ncbookmarkUtils = NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
            return AlertDialog.Builder(activity)
                .setTitle("Remove Bookmark ${bookmark.title}?")
                .setMessage("Title:${bookmark.title}\nURL:${bookmark.url}\nAre you sure remove this bookmark?" +
                        "\nRemember: this action cannot be undo!")
                .setNegativeButton("No, Cancel", null)
                .setPositiveButton("Yes, Remove", object : DialogInterface.OnClickListener{
                    override fun onClick(p0: DialogInterface?, p1: Int) {
                        ncbookmarkUtils.removeBookmark(
                            ncBookmarkMainController.ncUrl,
                            ncBookmarkMainController.username,
                            ncBookmarkMainController.password,
                            bookmark.folderId.toString(),{isSuccess, error ->
                                if(isSuccess){
                                    Toast.makeText(activity, "Bookmark Removed.", Toast.LENGTH_SHORT).show()
                                    ncBookmarkMainController.refreshList()
                                }else{
                                    Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                                }

                            })
                    }

                }).create()
        }
    }

}