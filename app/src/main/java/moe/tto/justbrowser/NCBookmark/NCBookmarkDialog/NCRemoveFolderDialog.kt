package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.*

class NCRemoveFolderDialog{
    companion object {
        fun create(activity: MainActivity,
                   ncBookmarkMainController: NCBookmarkMainController,
                   folderListItem:FloccusListviewItem,
                   onDialogCreateCallback:(dialog:AlertDialog)->Unit) {
            val ncbookmarkUtils = NCBookmarkUtils(activity, ncBookmarkMainController.ncUrl)
            ncbookmarkUtils.getBookmarks(folderListItem.uri,
                    ncBookmarkMainController.username,
                    ncBookmarkMainController.password,{bookmarks->
                    ncbookmarkUtils.fetchFolderList(ncBookmarkMainController.username, ncBookmarkMainController.password,{
                        val folderHelper = NCFolderHelper(activity, it)
                        val folders = folderHelper.getDisplayList(folderListItem.uri)
                        // callbackfor bookmark list
                        if((bookmarks!= null && bookmarks.isNotEmpty()) || folders.isNotEmpty()){
                            onDialogCreateCallback(
                             AlertDialog.Builder(activity)
                                .setTitle("Remove failed")
                                .setMessage("You can not remoe non-empty folder.")
                                .setNegativeButton("OK", null)
                                .create()
                            )
                        }else{
                            val dialog = AlertDialog.Builder(activity)
                                .setTitle("Remove folder ${folderListItem.title}")
                                .setMessage("Are you sure remove folder `${folderListItem.title}`?\nRemember: This action can not be undo!")
                                .setNegativeButton("No, Cancel", null)
                                .setPositiveButton("Yes, Remove", DialogInterface.OnClickListener { dialogInterface, i ->
                                    ncbookmarkUtils.removeFolder(
                                        ncBookmarkMainController.ncUrl,
                                        ncBookmarkMainController.username,
                                        ncBookmarkMainController.password,
                                        folderListItem.uri,{isSuccess, error ->
                                            if(isSuccess){
                                                ncBookmarkMainController.refreshList()
                                                Toast.makeText(activity, "Folder remove success!", Toast.LENGTH_SHORT).show()
                                            }else{
                                                Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
                                            }
                                        })
                                }).create()
                            onDialogCreateCallback(dialog)
                        }

                    })
                    })


        }

    }
}