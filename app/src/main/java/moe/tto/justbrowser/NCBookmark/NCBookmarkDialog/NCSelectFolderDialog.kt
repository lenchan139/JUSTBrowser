package moe.tto.justbrowser.NCBookmark.NCBookmarkDialog

import android.content.DialogInterface
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.NCBookmark.NCBookmarkMainController
import moe.tto.justbrowser.NCBookmark.NCFolderItem

class NCSelectFolderDialog{
        companion object {
            fun create(activity:MainActivity,
                       ncBookmarkMainController: NCBookmarkMainController,
                       folders:ArrayList<NCFolderItem>,
                       hideNewFolderButton:Boolean,
                       onItemClickCallback:(folderItem:NCFolderItem)->Unit,
                       onNewFolderCallback:(isSuccess:Boolean)->Unit):AlertDialog?{
                if(folders.isNotEmpty()){
                    val copyFolders = folders
                    val adapter = object : ArrayAdapter<NCFolderItem>(
                        activity,
                        android.R.layout.simple_list_item_1,
                        copyFolders.toTypedArray()){
                        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                            val view = super.getView(position, convertView, parent)
                            val line = view.findViewById<TextView>(android.R.id.text1)
                            line.setText(copyFolders[position].title)
                            return view
                        }
                    }
                    val dialog = AlertDialog.Builder(activity)
                        .setTitle("Folders:")
                        .setAdapter(adapter, DialogInterface.OnClickListener { dialogInterface, i ->
                            onItemClickCallback(copyFolders[i])
                        }).setNegativeButton("Cancel", null)

                        if(!hideNewFolderButton) {
                            dialog.setNeutralButton(
                                "New Folder",
                                DialogInterface.OnClickListener { dialogInterface, i ->
                                    onNewFolderCallback(true)
                                })
                        }
                    return dialog.create()
                }else{
                    return null
                }
            }
        }
}