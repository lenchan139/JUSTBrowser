package moe.tto.justbrowser.History

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.text.SimpleDateFormat
import java.util.*


class HistorySQLiteHelper(context: Context) : SQLiteOpenHelper(context,
    moe.tto.justbrowser.History.HistorySQLiteHelper.Companion._DBName, null,
    moe.tto.justbrowser.History.HistorySQLiteHelper.Companion._DBVersion
) {
    val sqlCreate = "CREATE TABLE " + moe.tto.justbrowser.History.HistorySQLiteHelper.Companion._TableName + "(" +
            "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "_title VARCHAR(199), " +
            "_url TEXT, " +
            "_addDate DATE " +
            ")"
    override fun onCreate(db: SQLiteDatabase) {

        db.execSQL(sqlCreate)
        val arrayToInit = arrayListOf<moe.tto.justbrowser.Item.Page>(
            moe.tto.justbrowser.Item.Page("https://google.com/", "Google"),
            moe.tto.justbrowser.Item.Page("https://duckduckgo.com/", "DuckDuckGo")
        )
        for(p in arrayToInit) {
            val sqlInsert = "INSERT INTO " + get_TableName() +
                    "(_title,_url,_addDate) VALUES(\"" +
                    p.title + "\",\"" + p.url + "\",\"" +
                    SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().time) + "\")"
            db.execSQL(sqlInsert)
        }


    }

    public fun get_TableName(): String {
        return moe.tto.justbrowser.History.HistorySQLiteHelper.Companion._TableName
    }
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        val sqlDrop = "DROP TABLE " + moe.tto.justbrowser.History.HistorySQLiteHelper.Companion._TableName
        db.execSQL(sqlDrop)

    }

    companion object {
        val _DBVersion = 1
        val _DBName = "HistoryList.db"
        val _TableName = "histroyItems"
    }
}
