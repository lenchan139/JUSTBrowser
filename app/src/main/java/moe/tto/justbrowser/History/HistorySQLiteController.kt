package moe.tto.justbrowser.History

import android.content.Context
import android.database.Cursor
import android.util.Log
import org.jetbrains.anko.doAsync
import java.text.SimpleDateFormat
import java.util.*


class HistorySQLiteController(private val context: Context) {
    private var historySQLiteHelper: moe.tto.justbrowser.History.HistorySQLiteHelper? = null

    val verifyKey: String
        get() = moe.tto.justbrowser.History.HistorySQLiteController.Companion.VERIFY_KEY
    val history: ArrayList<moe.tto.justbrowser.History.HistoryItem>
        get() {
            val result = ArrayList<moe.tto.justbrowser.History.HistoryItem>()
            val db = historySQLiteHelper!!.readableDatabase
            val sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() + " ORDER BY _id DESC"
            var cursor: Cursor? = null
            cursor = db.rawQuery(sqlSelct, null)
            Log.v("ss", cursor!!.count.toString())
            if (cursor.moveToFirst()) {
                do {
                    result.add(
                        moe.tto.justbrowser.History.HistoryItem(
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
                    )
                    Log.v("status", moe.tto.justbrowser.History.HistoryItem(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                    ).toString())
                } while (cursor.moveToNext())
            }
            cursor.close()
            return result
        }
    val lastHistory: HistoryItem?
        get() {
            var result : HistoryItem? = null
            val db = historySQLiteHelper!!.readableDatabase
            val sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() + " ORDER BY _id DESC LIMIT 1"
            var cursor: Cursor? = null
            cursor = db.rawQuery(sqlSelct, null)
            Log.v("ss", cursor!!.count.toString())
            if (cursor.moveToFirst()) {
                        result = HistoryItem(
                            cursor.getString(1),
                            cursor.getString(2),
                            cursor.getString(3)
                        )
            }
            cursor.close()
            return result
        }
    init {
        historySQLiteHelper = moe.tto.justbrowser.History.HistorySQLiteHelper(context)

    }

    fun removeAllHistory(verify_key: String): Boolean {
        val db = historySQLiteHelper!!.writableDatabase
        if (verify_key == moe.tto.justbrowser.History.HistorySQLiteController.Companion.VERIFY_KEY) {
            val sqlDrop = "DROP TABLE " + historySQLiteHelper!!.get_TableName()
            db.execSQL(sqlDrop)
            db.execSQL(historySQLiteHelper!!.sqlCreate)
            return true
        } else {
            return false
        }
    }

    fun addHistory(title: String, url: String) {
        val db = historySQLiteHelper!!.writableDatabase
        /*ContentValues values = new ContentValues();
        values.put("_title", title);
        values.put("_url", url);
        values.put("_addDate", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
        db.insert(historySQLiteHelper.get_TableName(), null, values);
        */
        doAsync {
            val lastHistory = lastHistory
            if(lastHistory != null && lastHistory.title.contains(title) && lastHistory.url.contains(url)) {
                Log.v("logHistroyAdd", "SKIP.")
            }else if (title.length > 0 && url.length > 0) {
                val sqlInsert = "INSERT INTO " + historySQLiteHelper!!.get_TableName() +
                        "(_title,_url,_addDate) VALUES(\"" +
                        title + "\",\"" + url + "\",\"" +
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(Calendar.getInstance().time) + "\")"
                Log.v("logHistroyAdd", sqlInsert)
                db.execSQL(sqlInsert)
            }
            db.close()
        }

    }

    fun getHistoryBySearchUrl(keyword: String?): ArrayList<moe.tto.justbrowser.History.HistoryItem> {
        keyword!!.replace(" ", "%")
        val result = ArrayList<moe.tto.justbrowser.History.HistoryItem>()
        val db = historySQLiteHelper!!.readableDatabase
        var sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() +
                " WHERE _url LIKE \"%" + keyword + "%\"" +
                " OR _title LIKE \"%" + keyword + "%\"" +
                " GROUP BY _title,_url" +
                " ORDER BY LENGTH(_url) + LENGTH(_title)*2 ASC"
        var cursor: Cursor? = null
        if (keyword == "") {
            sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() + " WHERE 2=1 "
        }
        cursor = db.rawQuery(sqlSelct, null)
        Log.v("ss", cursor!!.count.toString())
        if (cursor.moveToFirst()) {
            do {
                result.add(
                    moe.tto.justbrowser.History.HistoryItem(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                    )
                )
                Log.v("status", moe.tto.justbrowser.History.HistoryItem(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
                ).toString())
            } while (cursor.moveToNext())
        }
        cursor.close()
        return result
    }

    companion object {
        private val VERIFY_KEY = "sajidajoidajsfioajsfiovndb jduigrefjdksbv jueir4839fhie"
    }
}
