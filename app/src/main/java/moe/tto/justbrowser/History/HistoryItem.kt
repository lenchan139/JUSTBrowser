package moe.tto.justbrowser.History


class HistoryItem(var title: String, var url: String, var date: String) {

    override fun toString(): String {
        return "title=$title,url=$url,date=$date"
    }

}