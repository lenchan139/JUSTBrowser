package moe.tto.justbrowser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class InUrlActivity : AppCompatActivity() {

    override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_url_from_external);
        var inURL: String?
        if (intent.data != null) {
            Log.v("InUrlIntent", intent.data.toString())
            inURL = intent.data.toString()
            val intent = Intent(this, moe.tto.justbrowser.MainActivity::class.java)
            intent.putExtra(getString(R.string.key_stringExtra_InURL), inURL)
            intent.putExtra(getString(R.string.intentExtra_key_InURLFromExternal), true)
            startActivity(intent)
            inURL = null

        }
        finish()

    }

}
