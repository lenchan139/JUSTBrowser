package moe.tto.justbrowser

import android.util.Patterns

fun String.isValidUrl(): Boolean = Patterns.WEB_URL.matcher(this).matches()
fun String.takeLeft(i:Int): String = if(i>this.length || i<= 0) this else this.substring(0, i-1)