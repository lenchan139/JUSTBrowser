package moe.tto.justbrowser.Utils


import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import org.jetbrains.anko.doAsync

import java.io.BufferedReader
import java.io.Closeable
import java.io.IOException
import java.io.InputStreamReader
import java.net.URI
import java.net.URISyntaxException
import java.net.URL
import java.nio.charset.Charset
import java.util.HashSet
import java.util.Locale


class AdBlocker private constructor(val context: Context) {
    private val mBlockedDomainsList = HashSet<String>()
    private var mBlockAds: Boolean = false
    private var isAdBlockEnabled = false
    private val adBlockFastRules = ArrayList<Regex>()
    val adblockEnabled: Boolean
        get() {
            updatePreference()
            return isAdBlockEnabled
        }

    fun setAdblockEnabled(b: Boolean): Boolean {
        isAdBlockEnabled = b
        updatePreference()
        return isAdBlockEnabled
    }

    init {
        if (mBlockedDomainsList.isEmpty()) {
            loadHostsFile(context)
        }
        mBlockAds = isAdBlockEnabled
    }
    fun reloadBlockedHostFile(context:Context){
        mBlockedDomainsList.clear()
        loadHostsFile(context)
        initAdBlockFast()
        mBlockAds = isAdBlockEnabled

    }
    fun updatePreference() {
        mBlockAds = isAdBlockEnabled
    }

    fun getPrefHostUrl(context: Context): String {
        val settings = PreferenceManager.getDefaultSharedPreferences(context)
        //val preferenceUtils  = PreferenceUtils(context)
        //settings.getString(commonStrings.TAG_pref_adblocker_url(),"")
        return ""
    }
    /**
     * a method that determines if the given URL is an ad or not. It performs
     * a search of the URL's domain on the blocked domain hash set.
     * @param url the URL to check for being an ad
     * @return true if it is an ad, false if it is not an ad
     */
    fun isAd(url:String?):Boolean{
        if (!mBlockAds || url == null) {
            return false
        }
        val googleBlock = Regex.fromLiteral("google.com.+ads/ga-aud.+tid=UA-.+cid=")

        return isOnAdBlockFast(url) || isAdOnHostFile(url) || googleBlock.matches(url)
    }
    fun isAdOnHostFile(url: String): Boolean {
        if(url.isEmpty())
            return false
        val domain: String
        try {
            domain = getDomainName(url)
        } catch (e: URISyntaxException) {
            Log.d(TAG, "URL '$url' is invalid", e)
            return false
        }

        val isOnBlacklist = mBlockedDomainsList.contains(domain.toLowerCase(mLocale))
        if (isOnBlacklist) {
            Log.d(TAG, "URL '$url' is an ad")
        }
        return isOnBlacklist
    }
    private fun isOnAdBlockFast(url:String):Boolean{
        if(url.isNotEmpty()) {
            for (regex in adBlockFastRules) {
                val isMatched = regex.matches(url)
                if (isMatched) {
                    return true
                }
            }
        }
        return false
    }

    /**
     * This method reads through a hosts file and extracts the domains that should
     * be redirected to localhost (a.k.a. IP address 127.0.0.1). It can handle files that
     * simply have a list of hostnames to block, or it can handle a full blown hosts file.
     * It will strip out comments, references to the base IP address and just extract the
     * domains to be used
     * @param context the context needed to read the file
     */
    private fun loadHostsFile(context: Context){
        loadHostsFileFromUrl(context)

    }

    private fun loadHostsFileFromUrl(context: Context) {
        val thread = Thread(Runnable {
            val asset = context.assets
            var reader: BufferedReader? = null
            try {
                reader = URL(getPrefHostUrl(context)).readText(Charset.defaultCharset()).reader().buffered()
                for(obj in readLineForHostFile(reader)){
                    mBlockedDomainsList.add(obj)
                }

            }catch(e: OutOfMemoryError){
                loadHostsFileFromLocal(context)
                Log.e("adBlockError",e.message)
                Log.e("adBlockError", "catch error when loading host file from url, use default host file instead")
            }catch (e: Exception) {
                loadHostsFileFromLocal(context)
                Log.e("adBlockError",e.message)
                Log.e("adBlockError", "catch error when loading host file from url, use default host file instead")
            } finally {
                utilsClose(reader)
            }
        })
        thread.start()
    }

    private fun loadHostsFileFromLocal(context: Context) {
        val thread = Thread(Runnable {
            val asset = context.assets
            var reader: BufferedReader? = null
            try {
                reader = BufferedReader(InputStreamReader(
                    asset.open(BLOCKED_DOMAINS_LIST_FILE_NAME)))
                for( obj in readLineForHostFile(reader)){
                    mBlockedDomainsList.add(obj)
                }
            } catch (e: IOException) {
                Log.wtf(TAG, "Reading blocked domains list from file '"
                        + BLOCKED_DOMAINS_LIST_FILE_NAME + "' failed.", e)
            } finally {
                utilsClose(reader)
            }
        })
        thread.start()
    }
    fun readLineForHostFile(reader:BufferedReader?): HashSet<String> {
        var line : String
        val blockedDomainList = HashSet<String>()
        while (true) {
            line = reader?.readLine() ?: break
            if (!line.isEmpty() && !line.startsWith(COMMENT)) {
                line = line.replace(LOCAL_IP_V4, EMPTY)
                    .replace(LOCAL_IP_V4_ALT, EMPTY)
                    .replace(LOCAL_IP_V6, EMPTY)
                    .replace(TAB, EMPTY)
                val comment = line.indexOf(COMMENT)
                if (comment >= 0) {
                    line = line.substring(0, comment)
                }
                line = line.trim { it <= ' ' }
                if (!line.isEmpty() && line != LOCALHOST) {
                    while (line.contains(SPACE)) {
                        val space = line.indexOf(SPACE)
                        val host = line.substring(0, space)
                        blockedDomainList.add(host.trim { it <= ' ' })
                        line = line.substring(space, line.length).trim { it <= ' ' }
                    }
                    blockedDomainList.add(line.trim { it <= ' ' })
                }
            }
        }
        return blockedDomainList
    }
    fun initAdBlockFast(){
        doAsync {
            val asset = context.assets
            var reader: BufferedReader? = null
            try {
                reader = BufferedReader(InputStreamReader(
                    asset.open(ADBLOCK_FAST_BLOCK_LIST_FILE_NAME)))
                for( obj in readLineForHostFile(reader)){
                    if(obj.startsWith("! ")){
                        continue
                    }else{
                        val regex = Regex.fromLiteral(obj)
                        adBlockFastRules.add(regex)
                    }
                }
            } catch (e: IOException) {
                Log.wtf(TAG, "Reading blocked domains list from file '"
                        + ADBLOCK_FAST_BLOCK_LIST_FILE_NAME + "' failed.", e)
            } finally {
                utilsClose(reader)
            }
        }
    }
    companion object {

        private val TAG = "AdBlock"
        private val BLOCKED_DOMAINS_LIST_FILE_NAME = "hosts.txt"
        private val ADBLOCK_FAST_BLOCK_LIST_FILE_NAME = "adblockfast.rule"
        private val LOCAL_IP_V4 = "127.0.0.1"
        private val LOCAL_IP_V4_ALT = "0.0.0.0"
        private val LOCAL_IP_V6 = "::1"
        private val LOCALHOST = "localhost"
        private val COMMENT = "#"
        private val TAB = "\t"
        private val SPACE = " "
        private val EMPTY = ""
        private val mLocale = Locale.getDefault()
        private var mInstance: AdBlocker? = null

        fun getInstance(context: Context): AdBlocker {
            if (mInstance == null) {
                mInstance = AdBlocker(context)
            }
            return mInstance as AdBlocker
        }

        /**
         * Returns the probable domain name for a given URL
         * @param url the url to parse
         * @return returns the domain
         * @throws URISyntaxException throws an exception if the string cannot form a URI
         */
        @Throws(URISyntaxException::class)
        private fun getDomainName(url: String): String {
            var url = url
            val index = url.indexOf('/', 8)
            if (index != -1) {
                url = url.substring(0, index)
            }

            val uri = URI(url)
            val domain = uri.host ?: return url

            return if (domain.startsWith("www.")) domain.substring(4) else domain
        }

        fun utilsClose(closeable: Closeable?) {
            if (closeable == null)
                return
            try {
                closeable.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }
}