package moe.tto.justbrowser.Utils

import android.app.DownloadManager
import moe.tto.justbrowser.MainActivity
import android.net.Uri
import java.io.File
import android.app.NotificationManager
import android.content.*
import android.os.Environment
import android.util.Log
import android.webkit.CookieManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import moe.tto.justbrowser.R


class ContentUtils(val context: Context){
    private val downloadLimit = 3
    private val notificationId = -1
    private val MAX_PROGRESS_INT = 100
    val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    fun copyToClipboard(text:String){
        val clip = ClipData.newPlainText(context.getString(R.string.string_clip_form_just_browser), text)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(context, String.format(context.getString(R.string.string_clip_was_copied), text), Toast.LENGTH_SHORT).show()
    }
    fun copyFromClipboard(fallbackString:String?):String{
        val clipData = clipboard.getPrimaryClip()
        if(clipData != null && clipData.itemCount > 0) {
            // Get source text.
            val item = clipData.getItemAt(0);
            val text = item.getText ().toString()
            return text
        }
        return fallbackString ?: ""
    }
    fun downloadFileFromUrl(context:Context, fromUrl:String, toPath: String?){
        val request = DownloadManager.Request(
            Uri.parse(fromUrl))

        val cookie = CookieManager.getInstance().getCookie(fromUrl)
        request.addRequestHeader("Cookie", cookie)
        var path = toPath
        val extention = fromUrl.substring(fromUrl.lastIndexOf("."))
        val preName = fromUrl.substring(fromUrl.lastIndexOf("/"))
            .replace("/", "")
            .replace(extention, "")
            .replace("?", "")

        val filename = String.format("%s%s", preName, extention)
        if(path == null){
            path = String.format("%s", Environment.DIRECTORY_DOWNLOADS)
        }
        request.allowScanningByMediaScanner()
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) //Notify client once download is completed!
        request.setDestinationInExternalPublicDir(path,  filename)
        var downloadId = -1L
        object : BroadcastReceiver(){

            override fun onReceive(contxt: Context?, intent: Intent?) {
                val action = intent?.action
                val manager = context.getSystemService(AppCompatActivity.DOWNLOAD_SERVICE) as DownloadManager
                if(DownloadManager.ACTION_DOWNLOAD_COMPLETE == action && (downloadId >= 0) ){
                    val query = DownloadManager.Query()
                    query.setFilterById(downloadId)
                    val c = manager.query(query)
                    if (c.moveToFirst()) {
                        val columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS)
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            var uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
                            uriString = uriString.replace("file:///","")
                            val apkFile = File(uriString)

                            Log.v("onDownloadConplete",uriString)
                            if(apkFile.extension == "apk"){
                                //context.installApk(context,apkFile)
                            }
                            Toast.makeText(context, context.getString(R.string.string_download_success_message), Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
        //registerReceiver(onComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        request.setDescription(context.getString(R.string.string_download_from_this_app_name))
        request.allowScanningByMediaScanner()
        val dm = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadId = dm.enqueue(request)
        Toast.makeText(context, context.getString(R.string.string_downloading_dotdotdot), //To notify the Client that the file is being downloaded
            Toast.LENGTH_LONG).show()






    }
}