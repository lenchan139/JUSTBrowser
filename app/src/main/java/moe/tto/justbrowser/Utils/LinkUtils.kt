package moe.tto.justbrowser.Utils

import android.content.ComponentName
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.preference.PreferenceManager
import android.util.Log
import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R

class LinkUtils(val activity: MainActivity){
    fun isOpenExternal(url:String):Boolean{
        if(url.startsWith(activity.getString(R.string.prefix_intent_scheme)))
            return true
        val preIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        val targetedShareIntents = ArrayList<Intent>()
        val resInfo = activity.packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_ALL)


        for (resolveInfo in resInfo){
            Log.v("listV",resolveInfo.activityInfo.packageName)
            val packageName = resolveInfo.activityInfo.packageName

            val targetedShareIntent = Intent(Intent.ACTION_VIEW,Uri.parse(url))
            targetedShareIntent.setPackage(packageName)
            if(!packageName.contains(activity.applicationContext.packageName) && !isInBrwse(resolveInfo.activityInfo.packageName, url)){
                targetedShareIntents.add(targetedShareIntent)
                Log.v("listVTureFalse","True")
            }
        }
        if(targetedShareIntents.size >= 1) {
            return true
        }else{
            return false
        }
    }
    fun isInBrwse(packageName: String, url: String):Boolean{

        val intentFilter = IntentFilter()
        val outFilters =  ArrayList<IntentFilter>()
        val outActivities = ArrayList<ComponentName>()

        val filterIntent = Intent(Intent.ACTION_VIEW,Uri.parse(activity.getString(R.string.url_example_com)))
        val filterTargetedIntents = ArrayList<Intent>()
        val filteredInfo = activity.packageManager.queryIntentActivities(filterIntent, PackageManager.MATCH_ALL)

        for (info in filteredInfo) {
            val targetedShareIntent = Intent(Intent.ACTION_VIEW,Uri.parse(url))
            targetedShareIntent.setPackage(info.activityInfo.packageName)
            filterTargetedIntents.add(targetedShareIntent)
            Log.v("CATS_ ", info.activityInfo.packageName)
            if(packageName == info.activityInfo.packageName){
                return true
            }
        }
        return false
    }

    fun tryOpenExternal(activity:MainActivity, url: String) {
        Log.v("runToExternalUrl", url)
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
        val isAutoOpenExternal = activity.preferenceUtils.getIsAutoExternal()
        if (isOpenExternal(url) && isAutoOpenExternal) {
            activity.runToExternal(url)
            activity.decideOpenExternalButton(url)
        }else if(isOpenExternal(url)){
            activity.decideOpenExternalButton(url)
        }else{
            activity.decideOpenExternalButton(null)
        }
    }

}