package moe.tto.justbrowser.Utils

import moe.tto.justbrowser.MainActivity
import moe.tto.justbrowser.R
import org.jetbrains.anko.defaultSharedPreferences

class PreferenceUtils(val activity: MainActivity){
    private fun getString(id:Int):String{
        return activity.getString(id)
    }
    val prefenceManager = activity.defaultSharedPreferences
    fun getHomepage(): String {
        return prefenceManager.getString(getString(R.string.pref_key_homepage), null) ?: getString(R.string.pref_defaultValue_homepage)
    }
    fun getIsAutoExternal() : Boolean{
        return prefenceManager.getBoolean(getString(R.string.pref_key_auto_open_externally), false)
    }
    fun getSearchEngineReplacer():String{
        return prefenceManager.getString(getString(R.string.pref_key_search_engine), null) ?: getString(R.string.pref_defaultValue_search_engine_replacer)
    }
    fun getShareTitleLimitSize():Int{
        return (prefenceManager.getString(getString(R.string.pref_key_max_share_title_size), null) ?:"80").toInt()
    }
    fun getShareBodyFormat():Int{
        val array = activity.resources.getStringArray(R.array.pref_array_share_body_format)
        val str = prefenceManager.getString(getString(R.string.pref_key_share_body_style),array[0])
        val index = array.indexOf(str)
        return index
    }
    fun getIsFromCrash():Boolean{
        return prefenceManager.getBoolean(getString(R.string.key_start_from_app_crash), false)
    }
    fun getNCLoginUsername():String{
        return prefenceManager.getString(getString(R.string.pref_key_ncbookmark_login_username), null) ?: ""
    }
    fun getNCLoginPassword():String{
        return prefenceManager.getString(getString(R.string.pref_key_ncbookmark_login_password), null) ?: ""
    }
    fun getNCLoginUrl():String{
        return prefenceManager.getString(getString(R.string.pref_key_ncbookmark_login_url), null) ?: ""
    }
    fun setNCLoginUrl(newValue:String){
        prefenceManager.edit().putString(getString(R.string.pref_key_ncbookmark_login_url), newValue).apply()
    }

    fun setNCLoginUsername(newValue:String){
        prefenceManager.edit().putString(getString(R.string.pref_key_ncbookmark_login_username), newValue).apply()
    }

    fun setNCLoginPassword(newValue:String){
        prefenceManager.edit().putString(getString(R.string.pref_key_ncbookmark_login_password), newValue).apply()
    }
    fun cleanNCLoginUsernamePassswordUrl(){
        prefenceManager
            .edit()
            .remove(getString(R.string.pref_key_ncbookmark_login_url))
            .remove(getString(R.string.pref_key_ncbookmark_login_username))
            .remove(getString(R.string.pref_key_ncbookmark_login_password))
            .apply()
    }

    fun setLastBrowseUrl(newValue:String?){
        prefenceManager.edit().putString(getString(R.string.last_key_last_browse_url), newValue).apply()
    }
    fun getLastBrowseUrl():String{
        return prefenceManager.getString(getString(R.string.last_key_last_browse_url), null) ?: ""
    }
    var isEnableAdBlocker:Boolean = true
        get() {
            return true
        }
    var AdBlockerHostFileUrl:String = ""
        get(){
            return ""
    }
}