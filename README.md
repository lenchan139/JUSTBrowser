# JUST　Browser
This browser is making with GeckoView and implement what I need. 
Older JUST Browser: [https://gitlab.com/lenchan139/JUSTBrowserArchivedV1](https://gitlab.com/lenchan139/JUSTBrowserArchivedV1)    
About GeckoView: [https://wiki.mozilla.org/Mobile/GeckoView](https://wiki.mozilla.org/Mobile/GeckoView)    

# To-Do
- [x] Implement with GeckoView
- [x] Multi-Tabs
- [x] Search Engine
- [ ] Ads Blocker
- [ ] Implement webpage prompt delegate 
- [ ] Implement delegates
- [x] Browse history manager
- [x] App preferences manager
- [x] Share webpage
- [x] NextCloud Bookmark Viewer
- [x] Open URL to other app handler
- [ ] Selection adjusting
- [ ] Display JS console log 
- [x] Download file from web(no cookie)
- [ ] Download file from web(with cookie)
- [ ] Readibility mode
- [ ] Find in page
- [ ] UserAgent manager
- [ ] screenshot full webpage 

# Screenshots
 <img src="Screenshots/1.png" width="320">
 <img src="Screenshots/2.png" width="320">
 <img src="Screenshots/3.png" width="320">
 <img src="Screenshots/4.png" width="320">

# Download
You can download this application from Play Store or Aptoide. Aptoide may faster than the Play Store.
- [Play Store](https://play.google.com/apps/testing/moe.tto.justbrowser)
- [APK file](https://gitlab.com/lenchan139/JUSTBrowser/tags)

# Feedback
I doubt that who will use this application... :(
However, you can send feedback or feature request by [open issue on GitLab](https://gitlab.com/lenchan139/JUSTBrowser/issues).
Or, mailing to [lenchan139@tto.moe](mailto:lenchan139@tto.moe).

# License
Basically, JUSTBrowser is under GPLv2 but a few libary is under their lincense.
If you want to implement from this project, you should review those libraries's licenses.
You can go to ([https://www.gnu.org/licenses/old-licenses/gpl-2.0.html](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)) view the full content of the license.